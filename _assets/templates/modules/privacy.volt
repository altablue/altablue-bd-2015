		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="fourteen centered col clearing">
				    		<div class="whole col">
								<h3>Privacy Policy</h3>
								<p>Read our privacy policy.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col whole" id="overview">
							<p>Altablue Limited (“We”) are committed to protecting and respecting your privacy.</p>
							<p><strong>What does this policy cover?</strong></p>
							<p>This policy (together with our terms of use and any other documents referred to on it) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us.</p>
							<p>By submitting your personal information to us, or by using this website, you agree to the terms of this privacy policy, our Cookies Statement and our Terms of Use. Please ensure you read these documents before using our website.</p>
							<p><strong>What if I disagree with all or part of the terms of this policy?</strong></p>
							<p>We ask that you do not use alta-blue.com (our site) or submit any personal information unless you agree with the terms of this privacy policy. If you have any questions or wish to explore the possibility of submitting information to us other than via our site please email&nbsp;<a href="mailto:data.privacy@alta-blue.com">data.privacy@alta-blue.com</a> with the subject heading “Privacy Policy Query”.</p>
							<p><strong>Who is the data controller?</strong></p>
							<p>For the purpose of the Data Protection Act 1998 (the Act), the data controller is Altablue Limited a private company limited by shares registered in Jersey by the Jersey Financial Services Commission Companies Registry having its head office at La Motte Chambers, St Helier, Jersey with company registration No. 15271 and an establishment registered in Scotland under No. BR016918.</p>
							<p><strong>What information do you collect?</strong></p>
							<p>Information you submit: We will collect and process personal data (such as your name and contact details) and sensitive data (defined below) that you submit to us (whether submitted via our site, by email or telephone correspondence, or otherwise).</p>
							<p>You should only provide sensitive data (information about your racial/ethnic origin, political opinions, religious beliefs, trade union memberships, health, sexual life and the commission of offences and related proceedings) if you agree to our processing of this information in accordance with the terms of this privacy policy.</p>
							<p>Information we collect about you: Each time you visit our site we may automatically collect the following information:</p>
							<ul>
								<li>technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;</li>
								<li>information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our site (including date and time); vacancies you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call us.</li>
							</ul>
							<p><em>Accuracy of information you provide us</em></p>
							<p>You warrant that all personal data and information submitted by you to us (whether submitted via our site, by email or telephone correspondence or otherwise) is true, accurate, complete and not misleading.</p>
							<p><strong>How will your information be used?</strong></p>
							<p>Information you give to us: The personal data and sensitive data you provide will be stored, processed and disclosed by us to:</p>
							<ul>
								<li>provide recruitment services to you and to facilitate the recruitment process.</li>
								<li>assess your suitability for any vacancy which you have applied for and contact you about such vacancy.</li>
								<li>consider and contact you in relation to vacancies which you have not applied for but which we consider you may be suitable for, this may include vacancies outside the country in which you are based.</li>
								<li>permit our clients to assess your suitability for vacancies.</li>
								<li>enable us to send you job alerts and marketing information about our products and services.</li>
								<li>carry out our obligations arising from any contracts entered into between you and us or us and our client.</li>
								<li>answer your questions and enquiries.</li>
								<li>permit third parties to provide services to you or our clients as are necessary to facilitate the recruitment process, including medicals, skills testing and mandatory training courses.</li>
								<li>permit third parties to provide services to us or carry out functions on our behalf such as professional advisors and IT service providers.</li>
								<li>notify you about changes to our services.</li>
							</ul>
							<p><em>Information we collect about you. We will use this information:</em></p>
							<ul>
								<li>to administer our site and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes.</li>
								<li>to improve our site to ensure that content is presented in the most effective manner for you and for your computer.</li>
								<li>to allow you to participate in interactive features of our service, when you choose to do so.</li>
								<li>as part of our efforts to keep our site safe and secure.</li>
								<li>to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you.</li>
								<li>to make suggestions and recommendations to you and other users of our site about goods or services that may interest you or them.</li>
							</ul>
							<p>We may share your personal information with any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 of the UK Companies Act 2006.</p>
							<p>We may share your information with selected third parties including:</p>
							<ul>
								<li>Business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you.</li>
								<li>Analytics and search engine providers that assist us in the improvement and optimisation of our site.</li>
							</ul>
							<p>We may disclose your personal information to third parties:</p>
							<ul>
								<li>In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.</li>
								<li>If Altablue Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.</li>
								<li>If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our Terms of Use [or terms and conditions of business and other agreements; or to protect the rights, property, or safety of Altablue Limited, our clients, or others.</li>
							</ul>
							<p>This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</p>
							<p><strong>How long will your information be stored?</strong></p>
							<p>We will hold your information for as long as is necessary to comply with our legal and contractual obligations and to in accordance with our legitimate interests as data controller.</p>
							<p><strong>Cookies</strong></p>
							<p>A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer if you agree. Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site. For detailed information on the cookies we use and the purposes for which we use them see our Cookie Statement.</p>
							<p><strong>Where do we store your personal data?</strong></p>
							<p>The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area (“EEA”). It may also be processed by staff operating outside the EEA who work for us or one of our group companies or service providers. Such staff maybe engaged in, among other things, the assessment of your suitability for a vacancy, processing of your information in connection with facilitating the recruitment process and the provision of support services. By submitting your personal information, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
							<p><strong>Security</strong></p>
							<p>We have measures in place to keep your information secure. This information may be stored by us either manually or automatically. We take reasonable steps to ensure that the collection and use of your information is in accordance with the Act.</p>
							<p>Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>
							<p><strong>Sending information over the Internet</strong></p>
							<p>Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p>
							<p><strong>Your rights</strong></p>
							<p>You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by contacting us at <a href="mailto:info@alta-blue.com">info@alta-blue.com</a> or logging into your irecruitment account and changing your preferences.</p>
							<p>You have the right to review and amend any personal information held about you by us, for example, if you believe your details may be out of date or incorrect. You also have the right to withdraw your consent to the use of your personal information.</p>
							<p><strong>Access to Information</strong></p>
							<p>The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.</p>
							<p><strong>Links to other websites</strong></p>
							<p>Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.</p>
							<p><strong>Changes to our privacy policy</strong></p>
							<p>Any changes we may make to our privacy policy in the future will be posted on this page and such change shall be highlighted on our site. Please check back frequently to see any updates or changes to our privacy policy.</p>
							<p><strong>Severability</strong></p>
							<p>In the event that any or part of these terms of use shall be determined by any competent authority to be invalid, unlawful or unenforceable to any extent, such terms shall to that extent be severed from the remaining terms which shall continue to be valid and enforceable to the fullest extent permitted by law.</p>
							<p><strong>Applicable Law</strong></p>
							<p>This privacy policy and your use of this website are subject to and shall be construed in accordance with the laws of England and Wales, although we retain the right to bring proceedings against you for breach of these terms in your country of residence or any other relevant country.</p>
							<p><strong>Contact</strong></p>
							<p>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to <a href="mailto:info@alta-blue.com">info@alta-blue.com</a></p>
						</article>
					</div>
				</div>
			</section>
		</section>