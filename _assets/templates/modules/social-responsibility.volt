		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="fourteen centered col clearing">
				    		<div class="whole col">
								<h3>About Altablue</h3>
								<p>We are committed to being a social responsible business. Read about how we achieve this below.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col whole" id="overview">
							<h2>Social Responsibility</h2>
							<p>Being responsible and accountable is a core belief of our business here at Altablue and we believe our commitment to corporate responsibility shows this. We believe that, as a global company, we have clear objectives which ensure a safe working environment. This creates a positive impact on our people and the communities we work in and with.</p>
							<p>Our corporate responsibilities cover several key elements of our business:</p>
							<p><strong>Environment</strong></p>
							<p>We care about our environment and ensuring that what we do as a business supports the efforts being made to protect it. Examples of how we support these efforts are:</p>
							<ul>
								<li>Altablue promote a car-sharing scheme to travel back and forth to work.</li>
								<li>Staff are encouraged to walk or cycle to work.</li>
								<li>Business related travel is kept to a minimum utilizing public transport and webex/link conference facilities where at all possible.</li>
								<li>Supply chain and Procurement source from local vendors to reduce fuel costs whenever possible.</li>
								<li>Staff are encouraged to print double sided wherever possible and the printers/copiers are set on a double sided setting as a default.</li>
								<li>All printer toners are recycled.</li>
								<li>All lights and equipment are switched off at the end of each working day or when not needed.</li>
								<li>Computer monitors are flat screens instead of CRT which are much more energy efficient.</li>
								<li>Recycling areas are set up in our offices.</li>
							</ul>
							<p><strong>Working Environment</strong></p>
							<p>We promote and reward staff based on merit. Altablue is committed equal opportunity in employment and ensures that no applicant or employee receives less favourable treatment on the grounds of any protected characteristic. We comply with all local equality acts and laws in the regions we operate in.</p>
							<p><strong>Local Communities</strong></p>
							<p>We strongly believe in charity and helping those less fortunate. Our staff are regularly involved with local, national and international charities and we are proud to support them with their volunteering and fundraising activities. In addition, our office supplies contract is with WildHearts who give all of their profits to fund the work of the WildHearts Foundation.</p>
						</article>
					</div>
				</div>
			</section>
		</section>