		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="twelve centered col clearing">
				    		<figure class="three left col">
								<img src="/imgs/svgs/on-demand.svg"/>
				    		</figure>
				    		<div class="pushright thirteen col">
								<h3>Talent On-Demand</h3>
								<p>Our recruitment team will source great talent for you business when you need them the most.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<nav id="subLinks">
				<div class="container">
					<ul class="navLayout">
						<li class="quarter left gapless active"><a href="/services/talent-on-demand" data-router-name="services.on-demand">Talent On-Demand</a></li>
						<li class="quarter left gapless"><a href="/services/recruitment-process-outsourcing" data-router-name="services.recruitment-process-outsourcing">Recruitment Process Outsourcing</a></li>
						<li class="quarter left gapless"><a href="/services/contingent-workforce-solutions" data-router-name="services.contingent-workforce-solutions">Contigent Workforce</a></li>
						<li class="quarter left gapless"><a href="/services/executive-search" data-router-name="services.executive-search">Executive Search</a></li>
					</ul>
				</div>
			</nav>
			
			<nav class="mobile-only" id="tabs">
				<div class="container">
					<ul>
						<li class="active">
							<a href="javascript:void(0)" data-tabs="overview">overview</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-tabs="poc">contact</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-tabs="resources">resources</a>
						</li>
					</ul>
				</div>
			</nav>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col ten" id="overview">
							<h2>Talent when you need it most</h2>
							<p>Even with the best forward planning, people and projects can take you by surprise. Altablue’s On-Demand recruitment team will source the people you need, when you need them, without any up-front costs.</p>
							<p>At Altablue, we provide a transparent and honest recruitment service. We deliver on our promises by applying rigorous processes to ensure that the quality of the people we represent, and any opportunity we generate for them, is unmatched.</p>
							<p>Our team is building relationships with the best technical, professional and managerial talent in wide range of industries every day. These relationships, combined with our in-depth industry and expert recruitment knowledge, means we create meaningful career and business opportunities for both our clients and our candidates.</p>
							<p>We know great people. Let us introduce you.</p>
							<p><strong>Use On-Demand for:</strong></p>
							<ul>
								<li>Urgent positions.</li>
								<li>Short term surges in recruitment activity or when internal resources are stretched.</li>
								<li>Positions where advertising response is typically poor.</li>
								<li>Positions where advertising response creates administrative burden.</li>
								<li>You need access to high quality network of candidates.</li>
							</ul>
						</article>
						<aside class="six col left">
							<div class="card-box nopad" id="poc">
								<div class="contact-top whole gapless stretch" data-stretch="/imgs/poc/squares.jpg"></div>
								<div class="contact-bottom">
									<figure class="centered ten">
										<img src="/imgs/poc/lesley.jpg" />
									</figure>
									<p>Lesley DeJager</p>
									<p><em>Business Development Director</em></p>
									<address>
										<p>Wellheads Place</p>
										<p>Aberdeen, AB21 7GB</p>
										<p>United Kingdom</p>
									</address>
									<a href="#" class="button orange">contact lesley</a>
									<div class="mobile-only contactButtons clearing">
										<a href="tel:+441224 777956"><span class="icon icon-phone"></span>(+44) 1224 777956</a>
										<a href="mailto:lesley.dejager@alta-blue.com"><span class="icon icon-email"></span>Email Lesley DeJager</a>
									</div>
								</div>
							</div>
							<div class="downloads" id="resources">
								<h4 class="icon icon-cloud">downloads</h4>
								<hr/>
								<ul>
									<li><a href="#" class="icon icon-docs-grey">on-demand service line</a></li>
								</ul>
							</div>
						</aside>
					</div>
				</div>
			</section>
		</section>