		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="fourteen centered col clearing">
				    		<div class="whole col">
								<h3>Our Sectors</h3>
								<p>We work in a large selection of sectors.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col whole" id="overview">
							<p>Altablue provides recruitment services to clients in the following sectors:</p>
							<ul>
								<li>Primary Industry</li>
								<ul>
									<li>Oil and gas</li>
									<li>Mining and quarrying</li>
									<li>Power generation, transmission and distribution</li>
									<li>Renewables</li>
									<li>Bulk chemical production</li>
									<li>Agribusiness</li>
								</ul>
								<li>Engineering and construction</li>
								<ul>
									<li>Commercial construction</li>
									<li>High density residential building</li>
									<li>Building services</li>
									<li>Roads and structures</li>
									<li>Ports and rail</li>
									<li>Water and utilities</li>
									<li>Waste and environment</li>
								</ul>
								<li>Manufacturing</li>
								<ul>
									<li>Industrial, Aerospace and Automotive</li>
									<li>Chemicals / petrochemicals</li>
									<li>Pharmaceutical, Bioscience / life sciences</li>
								</ul>
								<li>Property</li>
								<ul>
									<li>Architecture</li>
									<li>Town planning</li>
									<li>Property development</li>
									<li>Property and facilities management</li>
									<li>Retail and commercial sales and leasing</li>
								</ul>
								<li>Science and technology</li>
								<ul>
									<li>Biology, ecology, environment and conservation</li>
									<li>Telecommunications</li>
									<li>Information technology</li>
								</ul>
							</ul>
							<p>Within our industry sectors, Altablue recruit for all business support, technical and managerial roles across the entire project / development and service or product lifecycle. Our dedicated search team recruit executive and “c” suite positions.</p>
						</article>
					</div>
				</div>
			</section>
		</section>