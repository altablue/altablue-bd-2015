		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="fourteen centered col clearing">
				    		<div class="whole col">
								<h3>Cookie Policy</h3>
								<p>Read about our cookie policy.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col whole" id="overview">
							<p>This website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site.</p>
							<p>cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer if you agree. Cookies contain information that is transferred to your computer’s hard drive. More information on controlling cookies can be found on www.aboutcookies.org.</p>
							<p>We use the following cookies:</p>
							<ul>
								<li>Strictly necessary cookies. These are cookies that are required for the operation of our website. They include, for example, cookies that enable you to log into secure areas of our website.</li>
								<li>Analytical/performance cookies. They allow us to recognise and count the number of visitors and to see how visitors move around our website when they are using it. This helps us to improve the way our website works, for example, by ensuring that users are finding what they are looking for easily.</li>
								<li>Functionality cookies. These are used to recognise you when you return to our website. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region).</li>
								<li>Targeting cookies. These cookies record your visit to our website, the pages you have visited and the links you have followed. We will use this information to make our website and the advertising displayed on it more relevant to your interests. We may also share this information with third parties for this purpose.</li>
							</ul>
							<p>You can find more information about the individual cookies we use and the purposes for which we use them in the table below:</p>
							<table>
							<thead>
							<tr>
							<td>Type</td>
							<td>Name</td>
							<td>Description</td>
							<td>Comments</td>
							</tr>
							</thead>
							<tbody>
							<tr>
							<td>Session Cookie</td>
							<td>PPSN5I</td>
							<td>Used in load balancing.</td>
							<td>Temporary, removed at the end of session</td>
							</tr>
							<tr>
							<td>Persistent Cookie</td>
							<td>__utma (Google Analytics)</td>
							<td>Tracks the first and last visit (including Days and Visit). It is a persistent cookie which remains on a computer for 2 years unless the cache is cleared.</td>
							<td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank">Google Privacy Policy</a></td>
							</tr>
							<tr>
							<td>Session cookie</td>
							<td>__utmb (Google Analytics)</td>
							<td>Records the exact arrival time of a visit. It is a session cookie which expires when the user leaves the website or closes the browser window.</td>
							<td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank">Google Privacy Policy</a></td>
							</tr>
							<tr>
							<td>Persistent cookie</td>
							<td>__utmc (Google Analytics)</td>
							<td>Records the exact arrival time of a visit. It is a session cookie which expires when the user leaves the website or closes the browser window.</td>
							<td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank">Google Privacy Policy</a></td>
							</tr>
							<tr>
							<td>Persistent cookie</td>
							<td>__utmc (Google Analytics)</td>
							<td>Tracks the exit time of a visit. It remains on a computer until 30 minutes after the users last page view on the web site.</td>
							<td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank">Google Privacy Policy</a></td>
							</tr>
							<tr>
							<td>Persistent cookie</td>
							<td>__utmz</td>
							<td>Tracks where the user entered the site from e.g. a search engine or similar website.</td>
							<td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank">Google Privacy Policy</a></td>
							</tr>
							<tr>
							<td>Persistent cookie</td>
							<td>__utmv (Google Analytics)</td>
							<td>Used for segmentation and data experimentation The __utmv cookie works with the __utmz cookie to improve cookie targeting capability.</td>
							<td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank">Google Privacy Policy</a></td>
							</tr>
							<tr>
							<td>Session Cookie</td>
							<td>ASPNET_SessionID</td>
							<td>A strictly necessary cookie which stores anonymous tokens and unique identifier that allows the site to identify different visitors as they move around the page.</td>
							<td></td>
							</tr>
							<tr>
							<td>Persistnet Cookie</td>
							<td>guest_id</td>
							<td>This cookie is used to identify a user to twitter.<p></p>
							<p>If you do not use or have a twitter account is as unique number to you and collects information about what you viewed which may for example appear if you later decided to create a twitter profile</p></td>
							<td><a href="https://twitter.com/privacy" target="_blank">Twitter Privacy Policy</a></td>
							</tr>
							</tbody>
							</table>
							<p>Please note that third parties (including, for example, advertising networks and providers of external services like web traffic analysis services) may also use cookies, over which we have no control.</p>
							<p>These cookies are likely to be analytical/performance cookies or targeting cookies. You block cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including essential cookies) you may not be able to access all or parts of our site.</p>
						</article>
					</div>
				</div>
			</section>
		</section>