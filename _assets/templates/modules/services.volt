		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="fourteen centered col clearing">
				    		<div class=" whole col">
								<h3>Our Services</h3>
								<p>We provide a wide range of recruitment specialist services that can be tailored to suit your business needs.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent" class="overview">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col whole" id="overview">
							<div class="whole col clearing">
								<div class="col half left">
									<figure class="fourteen centered col">
										<img src="#" />
									</figure>
									<div class="whole clearing">
										<h4>service title</h4>
										<p>text</p>
									</div>
									<a href="#" class="centered half">read more</a>
								</div>
								<div class="col half pushright">
									<figure class="fourteen centered col">
										<img src="#" />
									</figure>
									<div class="whole clearing">
										<h4>service title</h4>
										<p>text</p>
									</div>
									<a href="#" class="centered half">read more</a>
								</div>
							</div>
							<div class="whole col clearing">
								<div class="col half left">
									<figure class="fourteen centered col">
										<img src="#" />
									</figure>
									<div class="whole clearing">
										<h4>service title</h4>
										<p>text</p>
									</div>
									<a href="#" class="centered half">read more</a>
								</div>
								<div class="col half pushright">
									<figure class="fourteen centered col">
										<img src="#" />
									</figure>
									<div class="whole clearing">
										<h4>service title</h4>
										<p>text</p>
									</div>
									<a href="#" class="centered half">read more</a>
								</div>
							</div>
						</article>
					</div>
				</div>
			</section>
		</section>