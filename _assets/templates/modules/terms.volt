		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="fourteen centered col clearing">
				    		<div class="whole col">
								<h3>Terms of Use</h3>
								<p>Read our business terms of use.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col whole" id="overview">
							<p>This terms of use (together with the documents referred to in it) tells you the terms of use on which you may make use of our website alta-blue.com (our site), whether as a guest or a registered user. Use of our site includes accessing, browsing, or registering to use our site.</p>
							<p>Please read these terms of use carefully before you start to use our site, as these will apply to your use of our site. We recommend that you print a copy of this for future reference.</p>
							<p>By using our site, you confirm that you accept these terms of use and that you agree to comply with them. If you do not agree to these terms of use, you must not use our site. Other applicable terms</p>
							<ul>
								<li>Our Privacy Policy, which sets out the terms on which we process any personal data we collect from you, or that you provide to us. By using our site, you consent to such processing and you warrant that all data provided by you is accurate.</li>
								<li>Our Cookie Statement, which sets out information about the cookies on our site.</li>
							</ul>
							<p><strong>Information about us</strong></p>
							<p>alta-blue.com is a site operated by Altablue Limited (“We”). We are a private company limited by shares registered in Jersey by the Jersey Financial Services Commission Companies Registry having its head office at La Motte Chambers, St Helier, Jersey with company registration No. 15271 and an establishment registered in Scotland under No. BR016918</p>
							<p><strong>Changes to these terms</strong></p>
							<p>We may revise these terms of use at any time by amending this page.</p>
							<p>Please check this page from time to time to take notice of any changes we made, as they are binding on you.</p>
							<p><strong>Changes to our site</strong></p>
							<p>We may update our site from time to time, and may change the content at any time. However, please note that any of the content on our site may be out of date at any given time, and we are under no obligation to update it.</p>
							<p>We do not guarantee that our site, or any content on it, will be free from errors or omissions.</p>
							<p><strong>Accessing our site</strong></p>
							<p>Our site is made available free of charge. We do not guarantee that our site, or any content on it, will always be available or be uninterrupted. Access to our site is permitted on a temporary basis. We may suspend, withdraw, discontinue or change all or any part of our site without notice. We will not be liable to you if for any reason our site is unavailable at any time or for any period.</p>
							<p>You are responsible for making all arrangements necessary for you to have access to our site. You are also responsible for ensuring that all persons who access our site through your internet connection are aware of these terms of use and other applicable terms and conditions, and that they comply with them.</p>
							<p><strong>Prohibited uses</strong></p>
							<p>You may use our site only for lawful purposes. You may not use our site:</p>
							<ul>
								<li>In any way that breaches any applicable local, national or international law or regulation.</li>
								<li>In any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect.</li>
								<li>To send, knowingly receive, upload, download, use or re-use any material which does not comply with our content standards.</li>
								<li>To transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar solicitation (spam).</li>
							</ul>
							<p><strong>Your account and password</strong></p>
							<p>If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential. You must not disclose it to any third party.</p>
							<p>We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these terms of use.</p>
							<p><strong>Intellectual property rights</strong></p>
							<p>We are the owner or the licensee of all intellectual property rights in our site, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.</p>
							<p>You may print off one copy, and may download extracts, of any page(s) from our site for your personal use and you may draw the attention of others within your organisation to content posted on our site.</p>
							<p>You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text.</p>
							<p>Our status (and that of any identified contributors) as the authors of content on our site must always be acknowledged.</p>
							<p>You must not use any part of the content on our site for commercial purposes without obtaining a licence to do so from us or our licensors.</p>
							<p>If you print off, copy or download any part of our site in breach of these terms of use, your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</p>
							<p><strong>No reliance on information</strong></p>
							<p>The content on our site is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on our site.<br>
							Although we make reasonable efforts to update the information on our site, we make no representations, warranties or guarantees, whether express or implied, that the content on our site is accurate, complete or up-to-date. We do not guarantee to find you any.</p>
							<p><strong>Limitation of our liability</strong></p>
							<p>Nothing in these terms of use excludes or limits our liability for death or personal injury arising from our negligence, or our fraud or fraudulent misrepresentation, or any other liability that cannot be excluded or limited by English law.</p>
							<p>To the extent permitted by law, we exclude all conditions, warranties, representations or other terms which may apply to our site or any content on it, whether express or implied.</p>
							<p>We will not be liable to any user for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:</p>
							<ul>
								<li>use of, or inability to use, our site; or</li>
								<li>use of or reliance on any content displayed on our site.</li>
								<li>loss of profits, sales, business, or revenue;</li>
								<li>business interruption;</li>
								<li>loss of anticipated savings;</li>
								<li>loss of business opportunity, goodwill or reputation; or</li>
								<li>any indirect or consequential loss or damage.</li>
							</ul>
							<p>You agree not to use our site for any commercial or business purposes, and we have no liability to you for any loss of profit, loss of business, business interruption, or loss of business opportunity.</p>
							<p>We will not be liable for any loss or damage caused by a virus, distributed denial-of-service attack, or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of our site or to your downloading of any content on it, or on any website linked to it.</p>
							<p>We assume no responsibility for the content of websites linked on our site. Such links should not be interpreted as endorsement by us of those linked websites. We will not be liable for any loss or damage that may arise from your use of them.</p>
							<p>If you are a candidate for a vacancy, we do not guarantee that you will be interviewed or engaged by our client by virtue of apply for a vacancy via our site or otherwise. If you are engaged to provide services (indirectly or directly) by one of our clients, the terms of such engagement shall be subject to contract and we do not make any representation or warranty as to the final terms (including rate of pay and duration of assignment) that will be obtained on our site or otherwise.</p>
							<p>Viruses We do not guarantee that our site will be secure or free from bugs or viruses.</p>
							<p>You are responsible for configuring your information technology, computer programmes and platform in order to access our site. You should use your own virus protection software.</p>
							<p>You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.</p>
							<p><strong>Linking to our site</strong></p>
							<p>You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it.</p>
							<p>You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</p>
							<p>Our site must not be framed on any other site, nor may you create a link to any part of our site other than the home page.</p>
							<p>We reserve the right to withdraw linking permission without notice.</p>
							<p><strong>Third party links and resources in our site</strong></p>
							<p>Where our site contains links to other sites and resources provided by third parties, these links are provided for your information only.</p>
							<p>We have no control over the contents of those sites or resources.</p>
							<p><strong>Termination</strong></p>
							<p>We reserve the right at our absolute discretion to deny you access to this site at any time without explanation.</p>
							<p><strong>Severability</strong></p>
							<p>In the event that any or part of these terms of use shall be determined by any competent authority to be invalid, unlawful or unenforceable to any extent, such terms shall to that extent be severed from the remaining terms which shall continue to be valid and enforceable to the fullest extent permitted by law.</p>
							<p><strong>Applicable law</strong></p>
							<p>These terms and conditions and your use of this website are subject to and shall be construed in accordance with the laws of England and Wales, although we retain the right to bring proceedings against you for breach of these terms in your country of residence or any other relevant country.</p>
							<p><strong>Contact us</strong></p>
							<p>To contact us, please email <a href="mailto:info@alta-blue.com">info@alta-blue.com</a></p>
						</article>
					</div>
				</div>
			</section>
		</section>