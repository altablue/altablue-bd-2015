		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="fourteen centered col clearing">
				    		<div class=" whole col">
								<h3>About Altablue</h3>
								<p>We are a recruitment agency that focuses on creating lasting relationships with our clients.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col whole" id="overview">
							<h2>We keep recruitment simple.</h2>
							<p>Whether you need an executive hire, have multiple positions to fill or want to outsource your recruitment process, we can provide tailored solutions for all your recruitment needs. At Altablue our focus is to create long lasting relationships with our clients, candidates and contractors. We are advocates for our customers who tell us that our reliable, collaborative and simple approach delivers results.</p>
							<p>We support you to attract, develop and retain the best talent in your industry. We do this through innovative recruitment processes that delivers exceptional value and great return on investment.</p>
							<p>When you work with Altablue you are assured a transparent and honest service; we hold ourselves accountable to these standards.</p>
							<p>Find out what we can do for your business; have a look at our services, find out how we’ve significantly improved our customer’s recruitment processes or get in touch to arrange a chat.</p>
						</article>
					</div>
				</div>
			</section>
		</section>