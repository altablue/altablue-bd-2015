		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="twelve centered col clearing">
				    		<figure class="three left col">
								<img src="/imgs/svgs/on-demand.svg"/>
				    		</figure>
				    		<div class="pushright thirteen col">
								<h3>Executive Search</h3>
								<p>We help make those executive hires a quality choice.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<nav id="subLinks">
				<div class="container">
					<ul class="navLayout">
						<li class="quarter left gapless"><a href="/services/talent-on-demand" data-router-name="services.on-demand">Talent On-Demand</a></li>
						<li class="quarter left gapless"><a href="/services/recruitment-process-outsourcing" data-router-name="services.recruitment-process-outsourcing">Recruitment Process Outsourcing</a></li>
						<li class="quarter left gapless"><a href="/services/contigent-workforce-solutions" data-router-name="services.contingent-workforce-solutions">Contigent Workforce</a></li>
						<li class="quarter left gapless active"><a href="/services/executive-search" data-router-name="services.executive-search">Executive Search</a></li>
					</ul>
				</div>
			</nav>
			
			<nav class="mobile-only" id="tabs">
				<div class="container">
					<ul>
						<li class="active">
							<a href="javascript:void(0)" data-tabs="overview">overview</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-tabs="poc">contact</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-tabs="resources">resources</a>
						</li>
					</ul>
				</div>
			</nav>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col ten" id="overview">
							<p>One of the most important elements of any business is its leaders. Finding those leaders can be the toughest recruitment assignments. Fortunately, here at Altablue we have some of the best people for this task.</p>
							<p>We can offer a tailored solution that is discreet, transparent and cost effective. We rely on our teams years of experience in the Executive Search field, which allows us to find and engage candidates no one else can.</p>
							<p>Our Executive Search consultants are in constant contact with your industry’s current and future leaders. We use a wide variety of networking tools and advanced social sourcing methods to target the brightest and the best.</p>
							<p>Altablue’s Executive Search consultants will work tirelessly to provide the highest quality of candidates and ensure your executive hire is the right choice for your business.</p>
							<p><strong>Use Executive Search For:</strong></p>
							<ul>
								<li>Senior Appointments</li>
								<li>Confidential / Sensitive Appointments</li>
								<li>Technical Leaders</li>
								<li>Senior Management, Executive and ‘C’ Suite Positions</li>
							</ul>
						</article>
						<aside class="six col left">
							<div class="card-box nopad" id="poc">
								<div class="contact-top whole gapless stretch" data-stretch="/imgs/poc/squares.jpg"></div>
								<div class="contact-bottom">
									<figure class="centered ten">
										<img src="/imgs/poc/lesley.jpg" />
									</figure>
									<p>Lesley DeJager</p>
									<p><em>Business Development Director</em></p>
									<address>
										<p>Wellheads Place</p>
										<p>Aberdeen, AB21 7GB</p>
										<p>United Kingdom</p>
									</address>
									<a href="#" class="button orange">contact lesley</a>
									<div class="mobile-only contactButtons clearing">
										<a href="tel:+441224 777956"><span class="icon icon-phone"></span>(+44) 1224 777956</a>
										<a href="mailto:lesley.dejager@alta-blue.com"><span class="icon icon-email"></span>Email Lesley DeJager</a>
									</div>
								</div>
							</div>
							<div class="downloads" id="resources">
								<h4 class="icon icon-cloud">downloads</h4>
								<hr/>
								<ul>
									<li><a href="#" class="icon icon-docs-grey">on-demand service line</a></li>
								</ul>
							</div>
						</aside>
					</div>
				</div>
			</section>
		</section>