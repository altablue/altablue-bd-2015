		<section id="hero">
			<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
			  	<div class="copy container">
			    	<div class="twelve centered col"><h2>We Keep Recruitment Simple</h2><h4>Our focus is to create long lasting relationships with our clients, candidates and contractors. Our customers tell us our approach to recruitment delivers results</h4></div>
			    	<div class="whole clearing">
				    	<div class="half cta col left">
							<a href="#module1" id="startClick" class="button blue">find out how</a>
						</div>
						<div class="half cta col pushright">
							<a href="https://jobs.alta-blue.com" target="_blank" class="button orange">search jobs</a>
						</div>
					</div>
			  	</div>
			  	<div class="overlay"></div>
			</div>
		</section>

		<section id="module1">
			<div id="services" class="stretch"  data-stretch="imgs/section-bg/services.jpg">
				<div class="container">
					<section class="card-box wow fadeInRight left seven col" data-wow-offset="200">
						<p>Whether you need an executive hire, have multiple positions to fill or want to outsource your recruitment process, we can provide tailored solutions for all your recruitment needs.</p>
						<p>We do this through innovative recruitment processes that delivers exceptional value and great return on&nbsp;investment.</p>
						<p>Find out what we can do for your business.</p>
						<a href="#" class="centered blue button">view our services</a>
					</section>
				</div>
			</div>
		</section>

		<section id="module2">
			<div id="sectors" class="stretch" data-stretch="imgs/section-bg/sectors.jpg">
				<div class="container">
					<section class="threequarters col centered clearing cd-intro">
						<h4 class="cd-headline slide">
							<span>Sectors we currently operate in:</span>
							<span class="cd-words-wrapper">
								<b class="is-visible">renewables</b>
								<b>utilities</b>
								<b>construction</b>
							</span>
						</h4>
					</section>
				</div>
				<div class="overlay"></div>
			</div>
		</section>

		<section id="module3">
			<div id="cs" class="stretch"  data-stretch="imgs/section-bg/cs.jpg">
				<div class="container">
					<section class="card-box wow fadeInLeft pushright seven col" data-wow-offset="200">
						<p>When you work with us you are assured a transparent and honest service; We hold ourselves accountable to these standards.</p>
						<p>We support you to attract, develop and retain the best talent in your industry.</p>
						<p>Find out how we’ve significantly improved our customer’s recruitment processes.</p>
						<a href="#" class="centered blue button">view case studies</a>
					</section>
				</div>
			</div>
		</section>

		<section id="module4">
			<div id="contact" class="stretch"  data-stretch="imgs/section-bg/contact.jpg">
				<div class="container">
					<section class="card-box nopad wow fadeInUp whole centered col clearing" data-wow-offset="200">
						<div class="side-image">
						  <div class="images-wrapper seven"></div>
						    <div class="side-image-content content nine gapless clearing">
								<h5>Altablue - Australia</h5>
								<address><p>Level 5 171 Collins Street Melbourne Victoria 3000</p></address>
								<div class="deets seven col left clearing">
									<p class="whole clearing"><strong>Melbourne:</strong> <a href="tel:+441224777878">(+613)&nbsp;9211&nbsp;6420</a></p>
									<a href="#contactModal" class="button blue" data-toggle="modal">Contact Us</a>
								</div>
								<div class="deets seven left col clearing">
									<p class="whole clearing"><strong>Perth:</strong><br/><a href="tel:+441224373012">(+618)&nbsp;6314&nbsp;2926</a></p>
									<a href="#contactCallModal" class="button orange" data-toggle="modal">Call Me Back</a>
								</div>
								<div class="mobile-only">
									<p>Melbourne <a href="tel:+613 9211 6420">+613 9211 6420</a></p>
									<p>Perth <a href="tel:+618 6314 2926">+618 6314 2926</a></p>
									<p><a href="mailto:australiaenquiries@alta-blue.com">australiaenquiries@alta-blue.com</a></p>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="overlay"></div>
			</div>
		</section>

	    <!-- Modal HTML -->
	    <div id="contactModal" class="modal fade">
	        <div class="modal-dialog container">
	            <div class="modal-content fourteen centered">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title">Contact Altablue</h4>
	                    <p>If you would like more information about Altablue or the services we provide, please use the contact form.</p>
	                </div>
	                <div class="modal-body clearing">
	                    <form id="contactSubmit" method="POST">
	                    	<fieldset class="clearing whole">
								<label>Your Full Name
									<input type="text" name="fullName" />
								</label>
							</fieldset>
							<fieldset class="clearing whole">
								<label>Your Email
									<input type="text" name="emailAddress" />
								</label>
							</fieldset>
	                    	<fieldset class="clearing whole">
								<label>Phone Number
									<input type="text" name="phone" />
								</label>
							</fieldset>
							<fieldset class="clearing whole">
								<label>Your Company Name
									<input type="text" name="companyName" />
								</label>
							</fieldset>
							<fieldset class="clearing whole">
								<label>Your Message
									<textarea name="message"></textarea>
								</label>
							</fieldset>
		                   	<div class="pushright half col">
		                   		<button type="button" class="btn btn-default button blue" data-dismiss="modal">Close</button>
		                    	<button type="submit" id="contactButton" class="btn btn-primary button blue">Send</button>
		                    </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>

	    <!-- Modal HTML -->
	    <div id="contactCallModal" class="modal fade">
	        <div class="modal-dialog container">
	            <div class="modal-content fourteen centered">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title">call back request</h4>
	                    <p>If you would like us to give you a call back, please complete the form below and we will contact you using the details entered.</p>
	                </div>
	                <div class="modal-body clearing">
	                    <form id="contactCallSubmit" method="POST">
	                    	<fieldset class="clearing whole">
								<label>Your Full Name
									<input type="text" />
								</label>
							</fieldset>
	                    	<fieldset class="clearing whole">
								<label>Contactable Phone Number
									<input type="text" />
								</label>
							</fieldset>
							<div class="clearing">
								<fieldset class="left half col">
									<label>Date
										<input type="text" id="datepicker" />
									</label>
								</fieldset>
								<fieldset class="pushright col half">
									<label>Time
										<input type="text" id="timePicker" class="time ui-timepicker-input" autocomplete="off" />
									</label>
								</fieldset>
							</div>
	                    </form>
	                   	<div class="pushright half col">
	                   		<button type="button" class="btn btn-default button blue" data-dismiss="modal">Close</button>
	                    	<button type="submit" id="contactCallButton" class="btn btn-primary button blue">Send</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>

		<section id="module5">
			<div id="blog" class="stretch"  data-stretch="imgs/section-bg/blog.jpg">
				<div class="container">
					<section class="card-box wow fadeInDown nopad whole centered col clearing" data-wow-offset="200">
						<div class="side-image">
						    <div class="side-image-content content nine gapless clearing">
								<h5>Latest Blog Post</h5>
								<p><strong>WHATS HAPPENING – CONSTRUCTION</strong></p>
								<p><small>by Iain Hamilton</small></p>
								<p class="post">At Altablue we work alongside our candidates and clients in the construction industry daily. This means news that is important to you is important to us.</p>
								<a href="http://blog.alta-blue.com" target="_blank" class="button orange">read post</a>
							</div>
							<div class="images-wrapper seven"></div>
						</div>
					</section>
				</div>
			</div>
		</section>

		<section id="module5">
			<div id="jobs" class="stretch"  data-stretch="imgs/section-bg/jobs.jpg">
				<div class="container">
					<section class="card-box whole centered col wow fadeInLeft" data-wow-offset="200">
						<h5>Latest Jobs</h5>
						<p>Check out our latest available jobs straight from our job board:</p>
						<div class="table">
							<table>
								<colgroup>
									<col width="42%">
									<col width="42%">
									<col width="15%">
								</colgroup>
								<thead>
									<tr>
										<th><strong class="icon icon-tie">title</strong></th>
										<th><strong class="icon icon-pin">location</strong></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>general manager</td>
										<td>aberdeen, united kingdom</td>
										<td><a href="#" class="button orange pushright">view job</a></td>
									</tr>
									<tr>
										<td>hseq manger</td>
										<td>aberdeen, united kingdom</td>
										<td><a href="#" class="button orange pushright">view job</a></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="whole centered">
							<a href="https://jobs.alta-blue.com" target="_blank" class="button blue">View all jobs</a>
						</div>
					</section>
				</div>
				<div class="overlay"></div>
			</div>
		</section>

		<section id="module2">
			<div id="careers"  class="stretch"  data-stretch="imgs/section-bg/careers.jpg">
				<div class="container wow fadeIn">
					<section class="threequarters col centered">
						<h3>Be part of something better <a class="button blue" href="#">altablue careers</a></h3>
					</section>
				</div>
			</div>
		</section>