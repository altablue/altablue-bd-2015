<section id="hero">
			<div class="image-gradient-dynamic">
			  	<div class="hero-img"><img src="imgs/hero/hero-bg.jpg" /></div>
			  	<div class="overlay"></div>
			  	<div class="copy container">
			    	<div class="ten centered col"><h2>We Keep Recruitment Simple</h2><h4>Our focus is to create long lasting relationships with our clients, candidates and contractors. Our customers tell us our approach to recruitment delivers results</h4></div>
			    	<div class="quarter centered col clearing">
						<a href="#" class="half left col">find out how</a>
						<a class="hold pushright col">search jobs</a>
			    	</div>
			  	</div>
			</div>
		</section>

		<section id="module1">
			<div id="services">
				<figure><img src="#" /></figure>
				<div class="container">
					<section class="card-box left half col">
						<p>Whether you need an executive hire, have multiple positions to fill or want to outsource your recruitment process, we can provide tailored solutions for all your recruitment needs.</p>
						<p>We do this through innovative recruitment processes that delivers exceptional value and great return on investment.</p>
						<p>Find out what we can do for your business.</p>
						<a href="#">view our services</a>
					</section>
				</div>
			</div>
		</section>

		<section id="module2">
			<div id="sectors">
				<figure><img src="#" /></figure>
				<div class="container">
					<section class="half col centered">
						<h3 class="left threequarters col">Sectors we currently operate in:<h3>
						<ul class="col quarter pushright">
							<li>Renewables</li>
							<li>Life Science</li>
							<li>Construction</li>
						</ul>
					</section>
				</div>
			</div>
		</section>

		<section id="module3">
			<div id="cs">
				<figure><img src="#" /></figure>
				<div class="container">
					<section class="card-box pushright half col">
						<p>When you work with us you are assured a transparent and honest service; We hold ourselves accountable to these standards.</p>
						<p>We support you to attract, develop and retain the best talent in your industry.</p>
						<p>Find out how we’ve significantly improved our customer’s recruitment processes.</p>
						<a href="#">view case studies</a>
					</section>
				</div>
			</div>
		</section>

		<section id="module4">
			<div id="contact">
				<figure><img src="#" /></figure>
				<div class="container">
					<section class="card-box whole centered col">
						<figure class="seven left gapless"><img src="#" /></figure>
						<div class="content pushright nine gapless clearing">
							<h5>Altablue Headquarters</h5>
							<address><p>Wellheads Place Aberdeen United Kingdom AB21 7GB</p></address>
							<div class="third col left clearing">
								<p class="whole"><strong>Tel:</strong> <a href="tel:+441224777878">(+44) 01224 777878</a></p>
								<a href="#">Contact Us</a>
							<div class="third pushright col clearing">
								<p class="whole"><strong>Fax:</strong> <a href="tel:+441224373012">(+44) 01224 373012</a></p>
								<a href="#">Call Me Back</a>
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>

		<section id="module5">
			<div id="blog">
				<figure><img src="#" /></figure>
				<div class="container">
					<section class="card-box whole centered col">
						<div class="content left nine gapless clearing">
							<h5>Latest Blog Post</h5>
							<p><strong>WHATS HAPPENING – CONSTRUCTION</strong></p>
							<p><small>by Iain Hamilton</small></p>
							<p>At Altablue we work alongside our candidates and clients in the construction industry daily. This means news that is important to you is important to us.</p>
							<a href="#">read post</a>
						</div>
						<figure class="seven pushright gapless"><img src="#" /></figure>
					</section>
				</div>
			</div>
		</section>

		<section id="module5">
			<div id="blog">
				<figure><img src="#" /></figure>
				<div class="container">
					<section class="card-box whole centered col">
						<h5>Latest Jobs</h5>
						<p>Check out our latest available jobs straight from our job board:</p>
						<div class="table">
							<table>
								<thead>
									<tr>
										<th><strong>title</strong></th>
										<th><strong>location</strong></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>general manager</td>
										<td>aberdeen, united kingdom</td>
										<td><a href="#">veiw job</a></td>
									</tr>
									<tr>
										<td>hseq manger</td>
										<td>aberdeen, united kingdom</td>
										<td><a href="#">view job</a></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="three centered">
							<a href="#">View all jobs</a>
						</div>
					</section>
				</div>
			</div>
		</section>

		<section id="module2">
			<div id="sectors">
				<figure><img src="#" /></figure>
				<div class="container">
					<section class="half col centered">
						<h3 class="left threequarters col">Be part of something better:<h3>
						<a class="pushright col quarter" href="#">altablue careers</a>
					</section>
				</div>
			</div>
		</section>