		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="whole centered col clearing">
				    		<div class="pushright thirteen col">
								<h3>Search Results</h3>
								<p>Pages on our website that match your search query.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<section id="searchRes">
							<script>
							 (function() {
							   var cx = '005585113151212812630:gmjohstk8se';
							   var gcse = document.createElement('script');
							   gcse.type = 'text/javascript';
							   gcse.async = true;
							   gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							       '//cse.google.com/cse.js?cx=' + cx;
							   var s = document.getElementsByTagName('script')[0];
							   s.parentNode.insertBefore(gcse, s);
							 })();
							</script>
							<gcse:searchbox-only></gcse:searchbox-only>
							<script>
							 (function() {
							   var cx = '005585113151212812630:gmjohstk8se';
							   var gcse = document.createElement('script');
							   gcse.type = 'text/javascript';
							   gcse.async = true;
							   gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							       '//cse.google.com/cse.js?cx=' + cx;
							   var s = document.getElementsByTagName('script')[0];
							   s.parentNode.insertBefore(gcse, s);
							 })();
							</script>
							<gcse:searchresults-only></gcse:searchresults-only>
						</section>
					</div>
				</div>
			</section>
		</section>