		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="fourteen centered col clearing">
				    		<div class="whole col">
								<h3>Contact Us</h3>
								<p>Get in touch if you have any questions or any feedback about our business.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<section id="theContent" class="contact">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col whole" id="overview">
							<p>We’d love to welcome you to our offices, so if you want to drop by and have a chat about what we can do for your business, you can find us at the following location:</p>
							<div class="clearing whole gapless">
								<hr/>
								<figure class="col left seven">
									<a href="https://goo.gl/maps/v0tti"><img src="/imgs/card-imgs/au-office.jpg" /></a>
								</figure>
								<address class="pushright col nine">
									<p>Altablue</p>
									<p>Level 5/ 171 Collins Street</p>
									<p>Melbourne</p>
									<p>Victoria 3000</p>
									<p>Australia</p>
									<br/>
									<p>Melbourne <a href="tel:+613 9211 6420">+613 9211 6420</a></p>
									<p>Perth <a href="tel:+618 6314 2926">+618 6314 2926</a></p>
									<p><a href="mailto:australiaenquiries@alta-blue.com">australiaenquiries@alta-blue.com</a></p>
								</address>
							</div>
							<hr/>
							<p><strong>Feedback</strong></p>
							<p>At Altablue we believe in listening to our customers and we regular collect feedback to highlight where we can improve. We are committed to constant improvement and are not afraid to acknowledge when things go wrong and deal with it appropriately.</p>
							<form class="clearing">
		                    	<fieldset class="clearing whole">
									<label>Your Full Name
										<input type="text" name="fullName" />
									</label>
								</fieldset>
								<fieldset class="clearing whole">
									<label>Your Email
										<input type="text" name="emailAddress" />
									</label>
								</fieldset>
		                    	<fieldset class="clearing whole">
									<label>Phone Number
										<input type="text" name="phone" />
									</label>
								</fieldset>
								<fieldset class="clearing whole">
									<label>Your Company Name
										<input type="text" name="companyName" />
									</label>
								</fieldset>
								<fieldset class="clearing whole">
									<label>Your Message
										<textarea name="message"></textarea>
									</label>
								</fieldset>
	                   			<div class="pushright half col clearing">
			                   		<button type="reset" class="btn btn-default button blue" data-dismiss="modal">Clear</button>
			                    	<button type="submit" id="contactButton" class="button orange">Send</button>
			                    </div>
							</form>
						</article>
					</div>
				</div>
			</section>
		</section>