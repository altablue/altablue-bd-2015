
					<footer class="clearing">
						<section id="top-footer">
							<div class="container">
								<ul class="left col sixth">
									<li class="top"><strong>Services</strong></li>
									<li><a href="/services/on-demand" data-router-name="services.on-demand">on-demand</a></li>
									<li><a href="/services/contingent-workforce-solutions" data-router-name="services.contingent-workforce-solutions">Contingent Workforce Solutions</a></li>
									<li><a href="/services/recruitment-process-outsourcing" data-router-name="services.recruitment-process-outsourcing">Recruitment Process Outsourcing</a></li>
									<li><a href="/services/executive-search" data-router-name="services.executive-search">Executive Search</a></li>
								</ul>
								<ul class="left col sixth">
									<li class="top"><strong>case studies</strong></li>
									<li><a href="javascript:void(0)">wind farm manager</a></li>
									<li><a href="javascript:void(0)">senior wind analysis</a></li>
									<li><a href="javascript:void(0)">regional well integrity</a></li>
								</ul>
								<ul class="left col sixth">
									<li class="top"><strong>about altablue</strong></li>
									<li><a href="/about-altablue" data-router-name="about">our ethos</a></li>
									<li><a href="/social-responsibility" data-router-name="social-responsibility">social responsibility</a></li>
									<li><a href="/sectors" data-router-name="sectors">sectors</a></li>
								</ul>
								<ul class="left col sixth">
									<li class="top"><strong>contact</strong></li>
									<li><a href="/contact" data-router-name="contact">contact us</a></li>
									<li><a href="javascript:void(0)" class="icon icon-twitter">twitter</a></li>
									<li><a href="javascript:void(0)" class="icon icon-facebook">facebook</a></li>
									<li><a href="javascript:void(0)" class="icon icon-linkedin">linkedin</a></li>
									<li><a href="javascript:void(0)" class="icon icon-google">google+</a></li>
								</ul>
								<div class="search left col twosixths clearing">
									<p><strong>search</strong></p>
									<form id="searchForm" class="whole col clearing" method="get" action="http://cse.google.com/cse" id="searchbox_005585113151212812630:gmjohstk8se">
										<input type="hidden" name="cx" value="005585113151212812630:gmjohstk8se" />
										<input type="hidden" name="q" id="qField" />
										<input type="text" name="oq" id="oqField" class="threequarters left gapless icon icon-search" /><button class="quarter pushright gapless button" type="submit">search</button>
									</form>
									<div class="half left col">
										<div class="whole clearing">
											<p><strong>general enquiries</strong></p>
											<p><em>Tel: </em><a href="tel:+441224777878">(+44) 1224 777878</a></p>
											<p><em>Fax: </em><a href="tel:+441224373012">(+44) 1224 373012</a></p>
										</div>
										<div class="whole clearing">
											<p><strong>Invoice Enquiries</strong></p>
											<p><em>Tel: </em><a href="tel:+441224777850">(+44) 1224 777850</a></p>
										</div>
									</div>
									<div class="half pushright col">
										<div class="whole clearing">
											<p><strong>Contractor Care</strong></p>
											<p><em>Tel: </em><a href="tel:+441224777859">(+44) 1224 777859</a></p>
										</div>	
									</div>
								</div>
							</div>
						</section>
						<section id="bottom-footer">
							<div class="container">
								<div class="quarter col left">
									<p><small>&copy; Copyright 2015 Altablue. All Rights Reserved.</small></p>
								</div>
								<div class="quarter col pushright">
									<ul>
										<li><a href="/legal/terms">terms</a></li>
										<li><a href="/legal/privacy">privacy</a></li>
										<li><a href="/legal/cookies">cookies</a></li>
									</ul>
								</div>
							</div>
						</section>
					</footer>
				</div>	
			</div>

			<nav id="mp-menu" class="mp-menu">
				<div class="mp-level">
					<h2 class="">All Categories</h2>
					<ul>
						<li class="icon icon-arrow-left">
							<a class="" href="javascript:void(0)">Employers</a>
							<div class="mp-level">
								<h2 class="">Employers</h2>
								<a class="mp-back" href="javascript:void(0)">back</a>
								<ul>
									<li class="icon icon-arrow-left">
										<a class="" href="javascript:void(0)">Services</a>
										<div class="mp-level">
											<h2>Services</h2>
											<a class="mp-back" href="javascript:void(0)">back</a>
											<ul>
												<li><a href="/services/on-demand" data-router-name="services.on-demand">on-demand</a></li>
												<li><a href="/services/contingent-workforce-solutions" data-router-name="services.contingent-workforce-solutions">Contingent Workforce Solutions</a></li>
												<li><a href="/services/recruitment-process-outsourcing" data-router-name="services.recruitment-process-outsourcing">Recruitment Process Outsourcing</a></li>
												<li><a href="/services/executive-search" data-router-name="services.executive-search">Executive Search</a></li>
											</ul>
										</div>
									</li>
									<li class="icon icon-arrow-left">
										<a class="" href="javascript:void(0)">Case Studies</a>
										<div class="mp-level">
											<h2>Case Studies</h2>
											<a class="mp-back" href="javascript:void(0)">back</a>
											<ul>
												<li><a href="javascript:void(0)">Regional Well Integrity Manager</a></li>
												<li><a href="javascript:void(0)">Wind Farm Manager</a></li>
												<li><a href="javascript:void(0)">Senior Wind Analysis Consultant</a></li>
											</ul>
										</div>
									</li>
									<li class="icon icon-arrow-left">
										<a class="" href="javascript:void(0)">About Altablue</a>
										<div class="mp-level">
											<h2>About Altablue</h2>
											<a class="mp-back" href="javascript:void(0)">back</a>
											<ul>
												<li><a href="/about-altablue/" data-router-name="about">our ethos</a></li>
												<li><a href="/social-responsibility/" data-router-name="social-responsibility">social responsibility</a></li>
												<li><a href="/sectors/" data-router-name="sectors">sectors</a></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li><a href="https://jobs.alta-blue.com" target="_blank">Job Seekers</a></li>
						<li><a href="http://blog.alta-blue.com" target="_blank">News</a></li>
						<li><a href="/contact/" data-router-name="contact">Contact</a></li>
					</ul>
						
				</div>
			</nav>
		</div>
		<script type="text/javascript" data-main="/js/modules/app" src="/js/libs/requirejs/require.js"></script>


		<script>
		  /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-XXXXXX-XX', 'www.alta-blue.com');
		  ga('send', 'pageview');*/
		</script>

	</body>
</html>