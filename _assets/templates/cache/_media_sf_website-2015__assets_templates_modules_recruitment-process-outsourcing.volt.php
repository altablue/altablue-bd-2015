		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="twelve centered col clearing">
				    		<figure class="three left col">
								<img src="/imgs/svgs/on-demand.svg"/>
				    		</figure>
				    		<div class="pushright thirteen col">
								<h3>Recruitment Process Outsourcing</h3>
								<p>We help make those executive hires a quality choice.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<nav id="subLinks">
				<div class="container">
					<ul class="navLayout">
						<li class="quarter left gapless"><a href="/services/talent-on-demand" data-router-name="services.on-demand">Talent On-Demand</a></li>
						<li class="quarter left gapless active"><a href="/services/recruitment-process-outsourcing" data-router-name="services.recruitment-process-outsourcing">Recruitment Process Outsourcing</a></li>
						<li class="quarter left gapless"><a href="/services/contingent-workforce-solutions"  data-router-name="services.contingent-workforce-solutions">Contigent Workforce</a></li>
						<li class="quarter left gapless"><a href="/services/executive-search" data-router-name="services.executive-search">Executive Search</a></li>
					</ul>
				</div>
			</nav>
			
			<nav class="mobile-only" id="tabs">
				<div class="container">
					<ul>
						<li class="active">
							<a href="javascript:void(0)" data-tabs="overview">overview</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-tabs="poc">contact</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-tabs="resources">resources</a>
						</li>
					</ul>
				</div>
			</nav>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col ten" id="overview">
							<p>Outsourcing your recruitment process to Altablue will give you a lasting advantage by seamlessly integrating our expertise with your business. Operating under your brand and values, we will deliver measurable results, aligned with your business priorities.</p>
							<p>We are analysts, researchers and marketers.  We are skilled in attracting, assessing and negotiating to bring top talent to your business. It’s a unique skill set, and one that is difficult to find within your average human resources team.</p>
							<p>With these skills in your organisation, recruiting is much more than advertising and interviewing. We’ll help you create a strong employer brand, manage workforce and succession planning, and ensure an ongoing dialogue with the brightest talent in your industry. It’s about creating opportunities, not just filling vacancies.</p>
							<p>Your people are your business. Talk to us about how to get it right.</p>
							<p><strong>Use Recruitment Process Outsourcing to:</strong></p>
							<ul>
								<li>Receive a cost effective and scalable service</li>
								<li>Access a broad range of skills available within our team</li>
								<li>Benefit from our team’s knowledge across multiple industries</li>
								<li>Manage fluctuating demand with uninterrupted support</li>
							</ul>
						</article>
						<aside class="six col left">
							<div class="card-box nopad" id="poc">
								<div class="contact-top whole gapless stretch" data-stretch="/imgs/poc/squares.jpg"></div>
								<div class="contact-bottom">
									<figure class="centered ten">
										<img src="/imgs/poc/lesley.jpg" />
									</figure>
									<p>Lesley DeJager</p>
									<p><em>Business Development Director</em></p>
									<address>
										<p>Wellheads Place</p>
										<p>Aberdeen, AB21 7GB</p>
										<p>United Kingdom</p>
									</address>
									<a href="#" class="button orange">contact lesley</a>
									<div class="mobile-only contactButtons clearing">
										<a href="tel:+441224 777956"><span class="icon icon-phone"></span>(+44) 1224 777956</a>
										<a href="mailto:lesley.dejager@alta-blue.com"><span class="icon icon-email"></span>Email Lesley DeJager</a>
									</div>
								</div>
							</div>
							<div class="downloads" id="resources">
								<h4 class="icon icon-cloud">downloads</h4>
								<hr/>
								<ul>
									<li><a href="#" class="icon icon-docs-grey">on-demand service line</a></li>
								</ul>
							</div>
						</aside>
					</div>
				</div>
			</section>
		</section>