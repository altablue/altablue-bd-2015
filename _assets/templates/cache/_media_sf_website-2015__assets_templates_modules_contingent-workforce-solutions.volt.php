		<section id="services" class="servicesPage">
			<section id="hero">
				<div class="image-gradient-dynamic stretch"  data-stretch="imgs/hero/hero-bg.jpg">
				  	<div class="copy container">
				    	<div class="twelve centered col clearing">
				    		<figure class="three left col">
								<img src="/imgs/svgs/on-demand.svg"/>
				    		</figure>
				    		<div class="pushright thirteen col">
								<h3>Contingent Workforce Solutions</h3>
								<p>Contigent workforces are esstential to businesses. Let us help manage your workforce  efficiently.</p>
				    		</div>
				    	</div>
				  	</div>
				  	<div class="overlay"></div>
				</div>
			</section>

			<nav id="subLinks">
				<div class="container">
					<ul class="navLayout">
						<li class="quarter left gapless"><a href="/services/talent-on-demand" data-router-name="services.on-demand">Talent On-Demand</a></li>
						<li class="quarter left gapless"><a href="/services/recruitment-process-outsourcing" data-router-name="services.recruitment-process-outsourcing">Recruitment Process Outsourcing</a></li>
						<li class="quarter left gapless active"><a href="/services/contingent-workforce-solutions" data-router-name="services.contingent-workforce-solutions">Contigent Workforce</a></li>
						<li class="quarter left gapless"><a href="/services/executive-search" data-router-name="services.executive-search">Executive Search</a></li>
					</ul>
				</div>
			</nav>
			
			<nav class="mobile-only" id="tabs">
				<div class="container">
					<ul>
						<li class="active">
							<a href="javascript:void(0)" data-tabs="overview">overview</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-tabs="poc">contact</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-tabs="resources">resources</a>
						</li>
					</ul>
				</div>
			</nav>

			<section id="theContent">
				<div class="container">
					<div class="fourteen centered clearing">
						<article class="pushright col ten" id="overview">
							<p>Every business benefits from contractors, consultants and temporary workers. Whether you need a single worker or you have a large contingent workforce that you rely to deliver your business, we have a solution to ensure your contingent workforce is cost effective and quality assured.</p>
							<p>Contingent workers bring different challenges to permanent employees.  They bring you distinct legal and contractual obligations.  They operate under diverse pay scales, tax treatment and payment terms. They are transient and difficult to keep track of.  An inability to keep on top of these challenges can reduce their effectiveness and your return on investment.</p>
							<p>We have specialist contingent workforce teams in each of our locations, with experience in managing all types of worker engagement, from a single temp to an entire contingent workforce.  We take care of your contingent workforce and let you focus on developing your business.</p>
							<p>Whether under a vendor neutral Managed Service Program (MSP) or utilised as stand-alone services, Altablue offers a comprehensive suite of contingent workforce solutions, which can be fully customised to meet your needs.</p>
							<p><strong>Contractors On Demand</strong></p>
							<p>We source and recruit contingent workers with engineering, technical, managerial, and specialist business support skills across a wide range of industries.</p>
							<p><strong>Payroll Services</strong></p>
							We offer cost effective payroll solutions and assure full legislative compliance in all locations that we operate.  Our industry-leading, cloud based software offers online timesheet capability with advanced reporting and analytics.  We have a strong track-record and are trusted by several major international organisations to ensure flawless payroll delivery.</p>
							<p><strong>Managed Service Provision</strong></p>
							<p>We consolidate large and complex labour supply chains, driving performance up and cost down.  Click here for details.</p>
							<p>Use Contingent Workforce Solutions to:</p>
							<ul>
								<li>Hire short term staff to cover peaks in demand or project-based requirements</li>
								<li>Source specialist skills you don’t usually have in your business</li>
								<li>Maintain a flexible peripheral workforce</li>
								<li>Ensure compliance with all statutory and contractual requirements</li>
								<li>Easily manage multiple worker types</li>
								<li>Drive down costs in your contingent workforce</li>
							</ul>
						</article>
						<aside class="six col left">
							<div class="card-box nopad" id="poc">
								<div class="contact-top whole gapless stretch" data-stretch="/imgs/poc/squares.jpg"></div>
								<div class="contact-bottom">
									<figure class="centered ten">
										<img src="/imgs/poc/lesley.jpg" />
									</figure>
									<p>Lesley DeJager</p>
									<p><em>Business Development Director</em></p>
									<address>
										<p>Wellheads Place</p>
										<p>Aberdeen, AB21 7GB</p>
										<p>United Kingdom</p>
									</address>
									<a href="#" class="button orange">contact lesley</a>
									<div class="mobile-only contactButtons clearing">
										<a href="tel:+441224 777956"><span class="icon icon-phone"></span>(+44) 1224 777956</a>
										<a href="mailto:lesley.dejager@alta-blue.com"><span class="icon icon-email"></span>Email Lesley DeJager</a>
									</div>
								</div>
							</div>
							<div class="downloads" id="resources">
								<h4 class="icon icon-cloud">downloads</h4>
								<hr/>
								<ul>
									<li><a href="#" class="icon icon-docs-grey">on-demand service line</a></li>
								</ul>
							</div>
						</aside>
					</div>
				</div>
			</section>
		</section>