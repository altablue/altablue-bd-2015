				<nav class="mobile-only" id="trigger">
					<a href="javascript:void(0)" class="icon-menu icon">menu</a>
				</nav>
				<nav id="theMenu" class="half pushright gapless">	
					<ul>
						<li class="nav-link hover-menu">
							<a class="top-parent quarter col" href="javascript:void(0)">Employers</a>
							<div class="submenu col clearing">
								<ul class="col left whole links">
									<div id="slider">
										<div class="menuone half col left">
											<li><a href="/">Homepage</a></li>
											<li class="hover-menu">
												<a href="javascript:void(0)" id="servicesLink" data-menu="servicesMenu" class="hover-menu-item">Services</a>
											</li>
											<li class="hover-menu">
												<a href="javascript:void(0)" id="csLink" data-menu="cdMenu" class="hover-menu-item">Case Studies</a>
											</li>
											<li class="hover-menu">
												<a href="javascript:void(0)" id="aboutLink" data-menu="aboutMenu" class="hover-menu-item">About&nbsp;Us</a>
											</li>
											<li><a href="#">Contact Us</a></li>
										</div>
										<div class="menutwo half col pushright">
											<ul class="submenu whole col" id="servicesMenu">
												<li><a data-menu="servicesLink" href="javascript:void(0)" class="backLink">&larr; Services</a></li>
												<li><a href="/services/talent-on-demand" data-router-name="services.on-demand">Talent On-Demand</a></li>
												<li><a href="/services/recruitment-process-outsourcing" data-router-name="services.recruitment-process-outsourcing">Recruitment Process Outsourcing</a></li>
												<li><a href="/services/contingent-workforce-solutions" data-router-name="services.contingent-workforce-solutions">Contigent Workforce</a></li>
												<li><a href="/services/executive-search" data-router-name="services.executive-search">Executive Search</a></li>
											</ul>
											<ul class="submenu whole col" id="aboutMenu">
												<li><a data-menu="aboutLink" href="javascript:void(0)" class="backLink">&larr; About Us</a></li>
												<li><a href="/about-altablue/" data-router-name="about">Our Ethos</a></li>
												<li><a href="/social-responsibility/" data-router-name="social-responsibility">Social Responsibility</a></li>
												<li><a href="/sectors/" data-router-name="sectors">Our Sectors</a></li>
											</ul>
											<ul class="submenu whole col" id="cdMenu">
												<li><a data-menu="csLink" href="javascript:void(0)" class="backLink">&larr; Case Studies</a></li>
												<li><a href="javascript:void(0)">Regional Well Integrity Manager</a></li>
												<li><a href="javascript:void(0)">Wind Farm Manager</a></li>
												<li><a href="javascript:void(0)">Senior Wind Analysis Consultant</a></li>
											</ul>
										</div>
									</div>
								</ul>
							</div>
						</li>
						<li><a class="top-parent quarter col" href="https://jobs.alta-blue.com" target="_blank">Job Seekers</a></li>
						<li><a class="top-parent quarter col" href="http://blog.alta-blue.com" target="_blank">News</a></li>
						<li><a class="top-parent quarter col" data-router-name="contact" href="/contact/">Contact</a></li>
					</ul>
				</nav>