requirejs.config({
	baseUrl: '/_assets/js/modules',
	paths : {
		crossroads : '/_assets/js/libs/crossroads/dist/crossroads.min',
		signals : '/_assets/js/libs/crossroads/dev/lib/signals',
		Ajax : '/_assets/js/libs/ajax/dist/ajax.min',
		jquery : '/_assets/js/libs/jquery/dist/jquery.min',
		anystretch : '/_assets/js/libs/anystretch/jquery.anystretch.min',
		gmaps: '/_assets/js/libs/gmaps',
		async: '/_assets/js/libs/async/dist/async.min',
		wow: '/_assets/js/libs/wow/dist/wow',
		menuAim: '/_assets/js/libs/jquery.menu-aim',
		classie: '/_assets/js/libs/classie',
		mlpushmenu: '/_assets/js/libs/mlpushmenu',
		domReady: '/_assets/js/libs/domReady',
		modernizr: '/_assets/js/libs/modernizr.custom',
		Pikaday: '/_assets/js/libs/pikaday/pikaday',
		timepicker: '/_assets/js/libs/jt.timepicker/jquery.timepicker.min',
		underscore : '/_assets/js/libs/underscore/underscore-min'
	},
	shim: {
		anystretch: {
			deps: ["jquery"]
		},
		menuAim: {
			deps: ["jquery"]
		}
	}
});

var debug = true;

require(['homepage/Controllers/IndexController', 'routes', 'router'], function(IndexController, routes, router){
    IndexController.start();
});

require([ 'Ajax', 'crossroads', 'underscore'], function( Ajax, crossroads, underscore ) {
	if(debug){
		crossroads.routed.add(console.log, console);
	}
});

require([ 'Ajax', 'crossroads', 'jquery', 'underscore', 'routes','homepage/Controllers/IndexController','domReady'], function( Ajax, crossroads, jq, underscore, routes, IndexController, domReady) {
  	var ajax = new Ajax();
  	jq.noConflict( true );
  	var routeName = null;

	var partials = document.getElementById('ui-view');
	var a = document.querySelectorAll('a');

	for(i=0;i<a.length;i++){
	    a[i].onclick=function(e){
	    	routeName = this.dataset.routerName;
	    	if(typeof routeName != 'undefined'){
		        e.preventDefault();
		        if(this.href.split('/')[3]){
		        	crossroads.parse('/' + this.href.split('/')[3] + '/' + this.href.split('/').pop());
		    	}else{
		    		crossroads.parse('/' + this.href.split('/').pop());
		    	}
	    	}
	    }
	}

	var cache = {};

	crossroads.addRoute('/:section:/:page:', function(section,page){
		if(typeof routeName != 'undefined'){
			var cacheName = routeName.split('.').join("");
			console.log(section);
			console.log(page);
			if(typeof cache[cacheName] != "undefined" && typeof cache[cacheName] != null){
				partials.innerHTML = cache[cacheName];
				IndexController.start();
				var strUrl ='/';
	    		if(section){
	    			strUrl += section+'/';
	    			if(page){
						strUrl += page+'/';
	    			}
	    		}
				window.history.pushState("", "", strUrl);
				window.scrollTo(0,0);
	    	}else{
	    		ajax.get(routes[routeName].templateUri)
		    	.done(function(data){
		    		partials.innerHTML = data;
		    		cache[cacheName] = data;
		    		var strUrl ='/';
		    		if(section){
		    			strUrl += section+'/';
		    			if(page){
							strUrl += page+'/';
		    			}
		    		}
		    			IndexController.start();
			    		domReady(function () {
				    		window.history.pushState("", "", strUrl);
				    		window.scrollTo(0,0);
			    		});
		    	})
		    	.error(function(data){
		    		console.log('error - 404')
		    	});
	    	}
	    }
	});

});

define('routes', function(){

   	var routes = {
	"home":
	{
		"url": "/",
		"controller": "IndexController",
		"templateUri": "/_assets/templates/modules/home.volt"
	},
    "services.on-demand": 
    {
    	"url": "/services/talent-on-demand",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/on-demand.volt"
    },
    "services.contingent-workforce-solutions": 
    {
    	"url": "/services/contingent-workforce-solutions",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/contingent-workforce-solutions.volt"
    },
    "services.recruitment-process-outsourcing": 
    {
    	"url": "/services/recruitment-process-outsourcing",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/recruitment-process-outsourcing.volt"
    },
    "services.executive-search": 
    {
    	"url": "/services/executive-search",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/executive-search.volt"
    },
    "about": 
    {
    	"url": "/about-altablue/",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/about-altablue.volt"
    },
    "social-responsibility": 
    {
    	"url": "/social-responsibility/",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/social-responsibility.volt"
    },
    "contact": 
    {
        "url": "/contact/",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/contact.volt"
    },
    "sectors": 
    {
    	"url": "/sectors/",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/sectors.volt"
    },
    "legal.terms": 
    {
    	"url": "/legal/terms",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/terms.volt"
    },
    "legal.privacy": 
    {
    	"url": "/legal/privacy",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/privacy.volt"
    },
    "legal.cookies": 
    {
    	"url": "/legal/cookies-policy",
        "controller": "IndexController",
        "templateUri": "/_assets/templates/modules/cookie-policy.volt"
    }
};

	return routes;
});


define(['jquery', 'anystretch', 'homepage/Services/headline', 'wow','homepage/Services/mobmenu','homepage/Services/modal', 'Pikaday', 'timepicker', 'Ajax'], function( jq, anystretch, headline, wow, mobmenu, modal, Pikaday, timepicker, Ajax){

	function start(){

		var picker = new Pikaday({ field: document.getElementById('datepicker') });

		jq.noConflict( true );

		jq(function(){

			var menu;

			jq('#timePicker').timepicker({
			    'disableTimeRanges': [
			        ['12am', '8am'],
			        ['5pm', '12am']
			    ]
			});

			wow = new WOW(
				{

				}
			);
			wow.init();

			jq('.icon-docs-grey').hover(function(){
				jq(this).toggleClass('icon-docs');
			});

			jq('.stretch').anystretch();

			jq('#contactCallButton, #contactButton').on('click',function(){
				var ajax = new Ajax();
				var form = jq(this).parents('form');
				var formData = form.serialize();
				var data = {
					formOptions: formData
				}
				ajax.post('contactSubmit.php', data)
		    	.done(function(data){
					jq('#mp-pusher').append('<div class="alert alert-success">Your contact request has been submitted successfully. We will be in touch.</div>');
					jq('.alert').delay(3000).fadeOut();
					var form = jq(this).parents('form');
					jq('#contactModal').modal('hide');
					jq('#contactCallModal').modal('hide');
		    	})
		    	.error(function(data){
		    		jq('#mp-pusher').append('<div class="alert alert-error">We could not send your contact request. Please try again later.</div>');
		    	});
		    	return false;
			});

			if (jq("#theNav").is('*')) {
			    var elem = jq('#theNav');
			    var offset = elem.offset();
			    var topValue =  offset.top + elem.height() - 1;
			    var width = elem.width();
			    jq(window).scroll(function (event) {
			          var y = jq(this).scrollTop();
			            if (y >= topValue) {  
			                if (!jq('#theNav').hasClass('follow')){
			                	jq("#theNav").find("#logo").find('img').attr('src', '/imgs/svgs/logo-white.svg');
			                   	jq("#theNav").hide().addClass("follow").fadeIn(250);
			                }
			            } else {
				            jq(".follow").removeClass("follow");
				            jq("#theNav").find("#logo").find('img').attr('src', '/imgs/svgs/logo.svg');

			            }
			     });
			}

			jq('.hover-menu-item').on('click', function(){
				jq('#slider').addClass('slideLeft');
				/*jq('.menuone').removeClass().addClass('menuone');
				jq('.menutwo').removeClass().addClass('menutwo');
				jq(this).parents('.menuone').addClass('slideOutLeft animated');*/
				menu = jq(this).attr('data-menu');
				jq('#'+menu).fadeIn();
				//jq('.menutwo').addClass('slideInRight animated');*/
			});	

			jq('.backLink').on('click', function(){
				jq('#slider').removeClass('slideLeft');
				jq('#'+menu).fadeOut();
				/*jq('.menuone').removeClass().addClass('menuone');
				jq('.menutwo').removeClass().addClass('menutwo');
				jq('.menutwo').addClass('slideOutRight animated');
				jq('.menuone').show().addClass('slideInLeft animated');
				jq('#'+menu).hide();*/
			});

			jq('#trigger').on('click', function(){
				jq('body, html').toggleClass('noScroll');
			});
			jq('#searchForm').submit(function(){
				jq(this).find('#qField').val(jq(this).find('#oqField').val()).parent('#searchForm').submit();
				return false;
			});

			jq('#startClick').on('click', function(){
				jq('html,body').animate({
		          scrollTop: jq('#module1').offset().top
		        }, 1000);
		        return false;
			});

		});
	}

    return {
        start:start
    };

});
require(['homepage/Views/HomeView', 'jquery', 'anystretch', 'homepage/Services/headline', 'wow','homepage/Services/mobmenu'], function(HomeView, jq, anystretch, headline, wow, mobmenu){

	jq.noConflict( true );

	//console.log(wow);

	//new wow().init();

	jq(function(){

		jq('#tabs a').on('click', function(){
			jq('.active').removeClass('active');
			jq(this).parent('li').addClass('active');
			var tab = jq(this).attr('data-tabs');
			jq('#overview, #poc, #resources').animate({opacity: 0}, 100,function(){
				jq(this).hide();	
				jq('#'+tab).show().animate({opacity: 1}, 100); 
			});
		});
		
	});

});
require(['jquery'], function(jq){

	jq(document).ready(function($){
		//set animation timing
		var animationDelay = 2500,
			//loading bar effect
			barAnimationDelay = 3800,
			barWaiting = barAnimationDelay - 3000, //3000 is the duration of the transition on the loading bar - set in the scss/css file
			//letters effect
			lettersDelay = 50,
			//type effect
			typeLettersDelay = 150,
			selectionDuration = 500,
			typeAnimationDelay = selectionDuration + 800,
			//clip effect 
			revealDuration = 600,
			revealAnimationDelay = 1500;
		
		initHeadline();


		function initHeadline() {
			//initialise headline animation
			animateHeadline($('.cd-headline'));
		}

		function singleLetters($words) {
			$words.each(function(){
				var word = $(this),
					letters = word.text().split(''),
					selected = word.hasClass('is-visible');
				for (i in letters) {
					if(word.parents('.rotate-2').length > 0) letters[i] = '<em>' + letters[i] + '</em>';
					letters[i] = (selected) ? '<i class="in">' + letters[i] + '</i>': '<i>' + letters[i] + '</i>';
				}
			    var newLetters = letters.join('');
			    word.html(newLetters).css('opacity', 1);
			});
		}

		function animateHeadline($headlines) {
			var duration = animationDelay;
			$headlines.each(function(){
				var headline = $(this);
				
				if(headline.hasClass('loading-bar')) {
					duration = barAnimationDelay;
					setTimeout(function(){ headline.find('.cd-words-wrapper').addClass('is-loading') }, barWaiting);
				} else if (headline.hasClass('clip')){
					var spanWrapper = headline.find('.cd-words-wrapper'),
						newWidth = spanWrapper.width() + 10
					spanWrapper.css('width', newWidth);
				} else if (!headline.hasClass('type') ) {
					//assign to .cd-words-wrapper the width of its longest word
					var words = headline.find('.cd-words-wrapper b'),
						width = 0;
					words.each(function(){
						var wordWidth = $(this).width();
					    if (wordWidth > width) width = wordWidth;
					});
					headline.find('.cd-words-wrapper').css('width', width);
				};

				//trigger animation
				setTimeout(function(){ hideWord( headline.find('.is-visible').eq(0) ) }, duration);
			});
		}

		function hideWord($word) {
			var nextWord = takeNext($word);
			
			if($word.parents('.cd-headline').hasClass('type')) {
				var parentSpan = $word.parent('.cd-words-wrapper');
				parentSpan.addClass('selected').removeClass('waiting');	
				setTimeout(function(){ 
					parentSpan.removeClass('selected'); 
					$word.removeClass('is-visible').addClass('is-hidden').children('i').removeClass('in').addClass('out');
				}, selectionDuration);
				setTimeout(function(){ showWord(nextWord, typeLettersDelay) }, typeAnimationDelay);
			
			} else if($word.parents('.cd-headline').hasClass('letters')) {
				var bool = ($word.children('i').length >= nextWord.children('i').length) ? true : false;
				hideLetter($word.find('i').eq(0), $word, bool, lettersDelay);
				showLetter(nextWord.find('i').eq(0), nextWord, bool, lettersDelay);

			}  else if($word.parents('.cd-headline').hasClass('clip')) {
				$word.parents('.cd-words-wrapper').animate({ width : '2px' }, revealDuration, function(){
					switchWord($word, nextWord);
					showWord(nextWord);
				});

			} else if ($word.parents('.cd-headline').hasClass('loading-bar')){
				$word.parents('.cd-words-wrapper').removeClass('is-loading');
				switchWord($word, nextWord);
				setTimeout(function(){ hideWord(nextWord) }, barAnimationDelay);
				setTimeout(function(){ $word.parents('.cd-words-wrapper').addClass('is-loading') }, barWaiting);

			} else {
				switchWord($word, nextWord);
				setTimeout(function(){ hideWord(nextWord) }, animationDelay);
			}
		}

		function showWord($word, $duration) {
			if($word.parents('.cd-headline').hasClass('type')) {
				showLetter($word.find('i').eq(0), $word, false, $duration);
				$word.addClass('is-visible').removeClass('is-hidden');

			}  else if($word.parents('.cd-headline').hasClass('clip')) {
				$word.parents('.cd-words-wrapper').animate({ 'width' : $word.width() + 10 }, revealDuration, function(){ 
					setTimeout(function(){ hideWord($word) }, revealAnimationDelay); 
				});
			}
		}

		function hideLetter($letter, $word, $bool, $duration) {
			$letter.removeClass('in').addClass('out');
			
			if(!$letter.is(':last-child')) {
			 	setTimeout(function(){ hideLetter($letter.next(), $word, $bool, $duration); }, $duration);  
			} else if($bool) { 
			 	setTimeout(function(){ hideWord(takeNext($word)) }, animationDelay);
			}

			if($letter.is(':last-child') && $('html').hasClass('no-csstransitions')) {
				var nextWord = takeNext($word);
				switchWord($word, nextWord);
			} 
		}

		function showLetter($letter, $word, $bool, $duration) {
			$letter.addClass('in').removeClass('out');
			
			if(!$letter.is(':last-child')) { 
				setTimeout(function(){ showLetter($letter.next(), $word, $bool, $duration); }, $duration); 
			} else { 
				if($word.parents('.cd-headline').hasClass('type')) { setTimeout(function(){ $word.parents('.cd-words-wrapper').addClass('waiting'); }, 200);}
				if(!$bool) { setTimeout(function(){ hideWord($word) }, animationDelay) }
			}
		}

		function takeNext($word) {
			return (!$word.is(':last-child')) ? $word.next() : $word.parent().children().eq(0);
		}

		function takePrev($word) {
			return (!$word.is(':first-child')) ? $word.prev() : $word.parent().children().last();
		}

		function switchWord($oldWord, $newWord) {
			$oldWord.removeClass('is-visible').addClass('is-hidden');
			$newWord.removeClass('is-hidden').addClass('is-visible');
		}
	});
});
require(['classie','modernizr'], function(classie,modernizr){


		function extend( a, b ) {
			for( var key in b ) { 
				if( b.hasOwnProperty( key ) ) {
					a[key] = b[key];
				}
			}
			return a;
		}

		// taken from https://github.com/inuyaksa/jquery.nicescroll/blob/master/jquery.nicescroll.js
		function hasParent( e, id ) {
			if (!e) return false;
			var el = e.target||e.srcElement||e||false;
			while (el && el.id != id) {
				el = el.parentNode||false;
			}
			return (el!==false);
		}

		// returns the depth of the element "e" relative to element with id=id
		// for this calculation only parents with classname = waypoint are considered
		function getLevelDepth( e, id, waypoint, cnt ) {
			cnt = cnt || 0;
			if ( e.id.indexOf( id ) >= 0 ) return cnt;
			if( classie.has( e, waypoint ) ) {
				++cnt;
			}
			return e.parentNode && getLevelDepth( e.parentNode, id, waypoint, cnt );
		}

		// http://coveroverflow.com/a/11381730/989439
		function mobilecheck() {
			var check = false;
			(function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
			return check;
		}

		// returns the closest element to 'e' that has class "classname"
		function closest( e, classname ) {
			if( classie.has( e, classname ) ) {
				return e;
			}
			return e.parentNode && closest( e.parentNode, classname );
		}

		function mlPushMenu( el, trigger, options ) {
			this.el = el;
			this.trigger = trigger;
			this.options = extend( this.defaults, options );
			// support 3d transforms
			this.support = Modernizr.csstransforms3d;
			if( this.support ) {
				this._init();
			}
		}

		mlPushMenu.prototype = {
			defaults : {
				// overlap: there will be a gap between open levels
				// cover: the open levels will be on top of any previous open level
				type : 'overlap', // overlap || cover
				// space between each overlaped level
				levelSpacing : 40,
				// classname for the element (if any) that when clicked closes the current level
				backClass : 'mp-back'
			},
			_init : function() {
				// if menu is open or not
				this.open = false;
				// level depth
				this.level = 0;
				// the moving wrapper
				this.wrapper = document.getElementById( 'mp-pusher' );
				// the mp-level elements
				this.levels = Array.prototype.slice.call( this.el.querySelectorAll( 'div.mp-level' ) );
				// save the depth of each of these mp-level elements
				var self = this;
				this.levels.forEach( function( el, i ) { el.setAttribute( 'data-level', getLevelDepth( el, self.el.id, 'mp-level' ) ); } );
				// the menu items
				this.menuItems = Array.prototype.slice.call( this.el.querySelectorAll( 'li' ) );
				// if type == "cover" these will serve as hooks to move back to the previous level
				this.levelBack = Array.prototype.slice.call( this.el.querySelectorAll( '.' + this.options.backClass ) );
				// event type (if mobile use touch events)
				this.eventtype = mobilecheck() ? 'touchstart' : 'click';
				// add the class mp-overlap or mp-cover to the main element depending on options.type
				classie.add( this.el, 'mp-' + this.options.type );
				// initialize / bind the necessary events
				this._initEvents();
			},
			_initEvents : function() {
				var self = this;

				// the menu should close if clicking somewhere on the body
				var bodyClickFn = function( el ) {
					self._resetMenu();
					el.removeEventListener( self.eventtype, bodyClickFn );
				};

				// open (or close) the menu
				this.trigger.addEventListener( this.eventtype, function( ev ) {
					ev.stopPropagation();
					ev.preventDefault();
					if( self.open ) {
						self._resetMenu();
					}
					else {
						self._openMenu();
						// the menu should close if clicking somewhere on the body (excluding clicks on the menu)
						document.addEventListener( self.eventtype, function( ev ) {
							if( self.open && !hasParent( ev.target, self.el.id ) ) {
								bodyClickFn( this );
							}
						} );
					}
				} );

				// opening a sub level menu
				this.menuItems.forEach( function( el, i ) {
					// check if it has a sub level
					var subLevel = el.querySelector( 'div.mp-level' );
					if( subLevel ) {
						el.querySelector( 'a' ).addEventListener( self.eventtype, function( ev ) {
							ev.preventDefault();
							var level = closest( el, 'mp-level' ).getAttribute( 'data-level' );
							if( self.level <= level ) {
								ev.stopPropagation();
								classie.add( closest( el, 'mp-level' ), 'mp-level-overlay' );
								self._openMenu( subLevel );
							}
						} );
					}
				} );

				// closing the sub levels :
				// by clicking on the visible part of the level element
				this.levels.forEach( function( el, i ) {
					el.addEventListener( self.eventtype, function( ev ) {
						ev.stopPropagation();
						var level = el.getAttribute( 'data-level' );
						if( self.level > level ) {
							self.level = level;
							self._closeMenu();
						}
					} );
				} );

				// by clicking on a specific element
				this.levelBack.forEach( function( el, i ) {
					el.addEventListener( self.eventtype, function( ev ) {
						ev.preventDefault();
						var level = closest( el, 'mp-level' ).getAttribute( 'data-level' );
						if( self.level <= level ) {
							ev.stopPropagation();
							self.level = closest( el, 'mp-level' ).getAttribute( 'data-level' ) - 1;
							self.level === 0 ? self._resetMenu() : self._closeMenu();
						}
					} );
				} );	
			},
			_openMenu : function( subLevel ) {
				// increment level depth
				++this.level;

				// move the main wrapper
				var levelFactor = ( this.level - 1 ) * this.options.levelSpacing,
					translateVal = this.options.type === 'overlap' ? this.el.offsetWidth + levelFactor : this.el.offsetWidth;
				
				this._setTransform( 'translate3d(' + translateVal + 'px,0,0)' );

				if( subLevel ) {
					// reset transform for sublevel
					this._setTransform( '', subLevel );
					// need to reset the translate value for the level menus that have the same level depth and are not open
					for( var i = 0, len = this.levels.length; i < len; ++i ) {
						var levelEl = this.levels[i];
						if( levelEl != subLevel && !classie.has( levelEl, 'mp-level-open' ) ) {
							this._setTransform( 'translate3d(-100%,0,0) translate3d(' + -1*levelFactor + 'px,0,0)', levelEl );
						}
					}
				}
				// add class mp-pushed to main wrapper if opening the first time
				if( this.level === 1 ) {
					classie.add( this.wrapper, 'mp-pushed' );
					this.open = true;
				}
				// add class mp-level-open to the opening level element
				classie.add( subLevel || this.levels[0], 'mp-level-open' );
			},
			// close the menu
			_resetMenu : function() {
				this._setTransform('translate3d(0,0,0)');
				this.level = 0;
				// remove class mp-pushed from main wrapper
				document.getElementById("main").className = ''; //.replace('noScroll', '');
				console.log(document.getElementById("main").className);
				//classie.remove( document.body.wrapper, 'noScroll' );
				classie.remove( this.wrapper, 'mp-pushed' );
				this._toggleLevels();
				this.open = false;
			},
			// close sub menus
			_closeMenu : function() {
				var translateVal = this.options.type === 'overlap' ? this.el.offsetWidth + ( this.level - 1 ) * this.options.levelSpacing : this.el.offsetWidth;
				this._setTransform( 'translate3d(' + translateVal + 'px,0,0)' );
				this._toggleLevels();
			},
			// translate the el
			_setTransform : function( val, el ) {
				el = el || this.wrapper;
				el.style.WebkitTransform = val;
				el.style.MozTransform = val;
				el.style.transform = val;
			},
			// removes classes mp-level-open from closing levels
			_toggleLevels : function() {
				for( var i = 0, len = this.levels.length; i < len; ++i ) {
					var levelEl = this.levels[i];
					if( levelEl.getAttribute( 'data-level' ) >= this.level + 1 ) {
						classie.remove( levelEl, 'mp-level-open' );
						classie.remove( levelEl, 'mp-level-overlay' );
					}
					else if( Number( levelEl.getAttribute( 'data-level' ) ) == this.level ) {
						classie.remove( levelEl, 'mp-level-overlay' );
					}
				}
			}
		}

		// add to global namespace
		window.mlPushMenu = mlPushMenu;


	new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ), {
		type : 'cover'
	} );
});
require(['jquery'], function($){

  /* ========================================================================
   * Bootstrap: modal.js v3.3.4
   * http://getbootstrap.com/javascript/#modals
   * ========================================================================
   * Copyright 2011-2015 Twitter, Inc.
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * ======================================================================== */
    'use strict';
    // MODAL CLASS DEFINITION
    // ======================
    var Modal = function (element, options) {
      this.options             = options
      this.$body               = $(document.body)
      this.$element            = $(element)
      this.$dialog             = this.$element.find('.modal-dialog')
      this.$backdrop           = null
      this.isShown             = null
      this.originalBodyPad     = null
      this.scrollbarWidth      = 0
      this.ignoreBackdropClick = false
      if (this.options.remote) {
        this.$element
          .find('.modal-content')
          .load(this.options.remote, $.proxy(function () {
            this.$element.trigger('loaded.bs.modal')
          }, this))
      }
    }
    Modal.VERSION  = '3.3.4'
    Modal.TRANSITION_DURATION = 300
    Modal.BACKDROP_TRANSITION_DURATION = 150
    Modal.DEFAULTS = {
      backdrop: true,
      keyboard: true,
      show: true
    }
    Modal.prototype.toggle = function (_relatedTarget) {
      return this.isShown ? this.hide() : this.show(_relatedTarget)
    }
    Modal.prototype.show = function (_relatedTarget) {
      var that = this
      var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })
      this.$element.trigger(e)
      if (this.isShown || e.isDefaultPrevented()) return
      this.isShown = true
      this.checkScrollbar()
      this.setScrollbar()
      this.$body.addClass('modal-open')
      this.escape()
      this.resize()
      this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))
      this.$dialog.on('mousedown.dismiss.bs.modal', function () {
        that.$element.one('mouseup.dismiss.bs.modal', function (e) {
          if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
        })
      })
      this.backdrop(function () {
        var transition = $.support.transition && that.$element.hasClass('fade')
        if (!that.$element.parent().length) {
          that.$element.appendTo(that.$body) // don't move modals dom position
        }
        that.$element
          .show()
          .scrollTop(0)
        that.adjustDialog()
        if (transition) {
          that.$element[0].offsetWidth // force reflow
        }
        that.$element
          .addClass('in')
          .attr('aria-hidden', false)
        that.enforceFocus()
        var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })
        transition ?
          that.$dialog // wait for modal to slide in
            .one('bsTransitionEnd', function () {
              that.$element.trigger('focus').trigger(e)
            })
            .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
          that.$element.trigger('focus').trigger(e)
      })
    }
    Modal.prototype.hide = function (e) {
      if (e) e.preventDefault()
      e = $.Event('hide.bs.modal')
      this.$element.trigger(e)
      if (!this.isShown || e.isDefaultPrevented()) return
      this.isShown = false
      this.escape()
      this.resize()
      $(document).off('focusin.bs.modal')
      this.$element
        .removeClass('in')
        .attr('aria-hidden', true)
        .off('click.dismiss.bs.modal')
        .off('mouseup.dismiss.bs.modal')
      this.$dialog.off('mousedown.dismiss.bs.modal')
      $.support.transition && this.$element.hasClass('fade') ?
        this.$element
          .one('bsTransitionEnd', $.proxy(this.hideModal, this))
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        this.hideModal()
    }
    Modal.prototype.enforceFocus = function () {
      $(document)
        .off('focusin.bs.modal') // guard against infinite focus loop
        .on('focusin.bs.modal', $.proxy(function (e) {
          if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
            this.$element.trigger('focus')
          }
        }, this))
    }
    Modal.prototype.escape = function () {
      if (this.isShown && this.options.keyboard) {
        this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
          e.which == 27 && this.hide()
        }, this))
      } else if (!this.isShown) {
        this.$element.off('keydown.dismiss.bs.modal')
      }
    }
    Modal.prototype.resize = function () {
      if (this.isShown) {
        $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
      } else {
        $(window).off('resize.bs.modal')
      }
    }
    Modal.prototype.hideModal = function () {
      var that = this
      this.$element.hide()
      this.backdrop(function () {
        that.$body.removeClass('modal-open')
        that.resetAdjustments()
        that.resetScrollbar()
        that.$element.trigger('hidden.bs.modal')
      })
    }
    Modal.prototype.removeBackdrop = function () {
      this.$backdrop && this.$backdrop.remove()
      this.$backdrop = null
    }
    Modal.prototype.backdrop = function (callback) {
      var that = this
      var animate = this.$element.hasClass('fade') ? 'fade' : ''
      if (this.isShown && this.options.backdrop) {
        var doAnimate = $.support.transition && animate
        this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
          .appendTo(this.$body)
        this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
          if (this.ignoreBackdropClick) {
            this.ignoreBackdropClick = false
            return
          }
          if (e.target !== e.currentTarget) return
          this.options.backdrop == 'static'
            ? this.$element[0].focus()
            : this.hide()
        }, this))
        if (doAnimate) this.$backdrop[0].offsetWidth // force reflow
        this.$backdrop.addClass('in')
        if (!callback) return
        doAnimate ?
          this.$backdrop
            .one('bsTransitionEnd', callback)
            .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
          callback()
      } else if (!this.isShown && this.$backdrop) {
        this.$backdrop.removeClass('in')
        var callbackRemove = function () {
          that.removeBackdrop()
          callback && callback()
        }
        $.support.transition && this.$element.hasClass('fade') ?
          this.$backdrop
            .one('bsTransitionEnd', callbackRemove)
            .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
          callbackRemove()
      } else if (callback) {
        callback()
      }
    }
    // these following methods are used to handle overflowing modals
    Modal.prototype.handleUpdate = function () {
      this.adjustDialog()
    }
    Modal.prototype.adjustDialog = function () {
      var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight
      this.$element.css({
        paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
        paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
      })
    }
    Modal.prototype.resetAdjustments = function () {
      this.$element.css({
        paddingLeft: '',
        paddingRight: ''
      })
    }
    Modal.prototype.checkScrollbar = function () {
      var fullWindowWidth = window.innerWidth
      if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
        var documentElementRect = document.documentElement.getBoundingClientRect()
        fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
      }
      this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
      this.scrollbarWidth = this.measureScrollbar()
    }
    Modal.prototype.setScrollbar = function () {
      var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
      this.originalBodyPad = document.body.style.paddingRight || ''
      if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
    }
    Modal.prototype.resetScrollbar = function () {
      this.$body.css('padding-right', this.originalBodyPad)
    }
    Modal.prototype.measureScrollbar = function () { // thx walsh
      var scrollDiv = document.createElement('div')
      scrollDiv.className = 'modal-scrollbar-measure'
      this.$body.append(scrollDiv)
      var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
      this.$body[0].removeChild(scrollDiv)
      return scrollbarWidth
    }
    // MODAL PLUGIN DEFINITION
    // =======================
    function Plugin(option, _relatedTarget) {
      return this.each(function () {
        var $this   = $(this)
        var data    = $this.data('bs.modal')
        var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)
        if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
        if (typeof option == 'string') data[option](_relatedTarget)
        else if (options.show) data.show(_relatedTarget)
      })
    }
    var old = $.fn.modal
    $.fn.modal             = Plugin
    $.fn.modal.Constructor = Modal
    // MODAL NO CONFLICT
    // =================
    $.fn.modal.noConflict = function () {
      $.fn.modal = old
      return this
    }
    // MODAL DATA-API
    // ==============
    $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
      var $this   = $(this)
      var href    = $this.attr('href')
      var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
      var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())
      if ($this.is('a')) e.preventDefault()
      $target.one('show.bs.modal', function (showEvent) {
        if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
        $target.one('hidden.bs.modal', function () {
          $this.is(':visible') && $this.trigger('focus')
        })
      })
      Plugin.call($target, option, this)
    })
});

define(function(){
     
    function render(parameters){
        var appDiv = document.getElementById('app');
 
        var users = [{name: 'test'}, {name: '123'}];
         
        var html = '<ul>';
        for (var i = 0, len = users.length; i < len; i++){
            html += '<li>' + users[i].name + '</li>';
        }
        html += '</ul>';
         
        appDiv.innerHTML = html;
    }
 
    return {
        render:render
    };
});