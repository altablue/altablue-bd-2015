define(['jquery', 'anystretch', 'homepage/Services/headline', 'wow','homepage/Services/mobmenu','homepage/Services/modal', 'Pikaday', 'timepicker', 'Ajax'], function( jq, anystretch, headline, wow, mobmenu, modal, Pikaday, timepicker, Ajax){

	function start(){

		var picker = new Pikaday({ field: document.getElementById('datepicker') });

		jq.noConflict( true );

		jq(function(){

			var menu;

			jq('#timePicker').timepicker({
			    'disableTimeRanges': [
			        ['12am', '8am'],
			        ['5pm', '12am']
			    ]
			});

			wow = new WOW(
				{

				}
			);
			wow.init();

			jq('.icon-docs-grey').hover(function(){
				jq(this).toggleClass('icon-docs');
			});

			jq('.stretch').anystretch();

			jq('#contactCallButton, #contactButton').on('click',function(){
				var ajax = new Ajax();
				var form = jq(this).parents('form');
				var formData = form.serialize();
				var data = {
					formOptions: formData
				}
				ajax.post('contactSubmit.php', data)
		    	.done(function(data){
					jq('#mp-pusher').append('<div class="alert alert-success">Your contact request has been submitted successfully. We will be in touch.</div>');
					jq('.alert').delay(3000).fadeOut();
					var form = jq(this).parents('form');
					jq('#contactModal').modal('hide');
					jq('#contactCallModal').modal('hide');
		    	})
		    	.error(function(data){
		    		jq('#mp-pusher').append('<div class="alert alert-error">We could not send your contact request. Please try again later.</div>');
		    	});
		    	return false;
			});

			if (jq("#theNav").is('*')) {
			    var elem = jq('#theNav');
			    var offset = elem.offset();
			    var topValue =  offset.top + elem.height() - 1;
			    var width = elem.width();
			    jq(window).scroll(function (event) {
			          var y = jq(this).scrollTop();
			            if (y >= topValue) {  
			                if (!jq('#theNav').hasClass('follow')){
			                	jq("#theNav").find("#logo").find('img').attr('src', '/imgs/svgs/logo-white.svg');
			                   	jq("#theNav").hide().addClass("follow").fadeIn(250);
			                }
			            } else {
				            jq(".follow").removeClass("follow");
				            jq("#theNav").find("#logo").find('img').attr('src', '/imgs/svgs/logo.svg');

			            }
			     });
			}

			jq('.hover-menu-item').on('click', function(){
				jq('#slider').addClass('slideLeft');
				/*jq('.menuone').removeClass().addClass('menuone');
				jq('.menutwo').removeClass().addClass('menutwo');
				jq(this).parents('.menuone').addClass('slideOutLeft animated');*/
				menu = jq(this).attr('data-menu');
				jq('#'+menu).fadeIn();
				//jq('.menutwo').addClass('slideInRight animated');*/
			});	

			jq('.backLink').on('click', function(){
				jq('#slider').removeClass('slideLeft');
				jq('#'+menu).fadeOut();
				/*jq('.menuone').removeClass().addClass('menuone');
				jq('.menutwo').removeClass().addClass('menutwo');
				jq('.menutwo').addClass('slideOutRight animated');
				jq('.menuone').show().addClass('slideInLeft animated');
				jq('#'+menu).hide();*/
			});

			jq('#trigger').on('click', function(){
				jq('body, html').toggleClass('noScroll');
			});
			jq('#searchForm').submit(function(){
				jq(this).find('#qField').val(jq(this).find('#oqField').val()).parent('#searchForm').submit();
				return false;
			});

			jq('#startClick').on('click', function(){
				jq('html,body').animate({
		          scrollTop: jq('#module1').offset().top
		        }, 1000);
		        return false;
			});

		});
	}

    return {
        start:start
    };

});