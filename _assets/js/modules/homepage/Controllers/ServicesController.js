require(['homepage/Views/HomeView', 'jquery', 'anystretch', 'homepage/Services/headline', 'wow','homepage/Services/mobmenu'], function(HomeView, jq, anystretch, headline, wow, mobmenu){

	jq.noConflict( true );

	//console.log(wow);

	//new wow().init();

	jq(function(){

		jq('#tabs a').on('click', function(){
			jq('.active').removeClass('active');
			jq(this).parent('li').addClass('active');
			var tab = jq(this).attr('data-tabs');
			jq('#overview, #poc, #resources').animate({opacity: 0}, 100,function(){
				jq(this).hide();	
				jq('#'+tab).show().animate({opacity: 1}, 100); 
			});
		});
		
	});

});