
require([ 'Ajax', 'crossroads', 'jquery', 'underscore', 'routes','homepage/Controllers/IndexController','domReady'], function( Ajax, crossroads, jq, underscore, routes, IndexController, domReady) {
  	var ajax = new Ajax();
  	jq.noConflict( true );
  	var routeName = null;

	var partials = document.getElementById('ui-view');
	var a = document.querySelectorAll('a');

	for(i=0;i<a.length;i++){
	    a[i].onclick=function(e){
	    	routeName = this.dataset.routerName;
	    	if(typeof routeName != 'undefined'){
		        e.preventDefault();
		        if(this.href.split('/')[3]){
		        	crossroads.parse('/' + this.href.split('/')[3] + '/' + this.href.split('/').pop());
		    	}else{
		    		crossroads.parse('/' + this.href.split('/').pop());
		    	}
	    	}
	    }
	}

	var cache = {};

	crossroads.addRoute('/:section:/:page:', function(section,page){
		if(typeof routeName != 'undefined'){
			var cacheName = routeName.split('.').join("");
			console.log(section);
			console.log(page);
			if(typeof cache[cacheName] != "undefined" && typeof cache[cacheName] != null){
				partials.innerHTML = cache[cacheName];
				IndexController.start();
				var strUrl ='/';
	    		if(section){
	    			strUrl += section+'/';
	    			if(page){
						strUrl += page+'/';
	    			}
	    		}
				window.history.pushState("", "", strUrl);
				window.scrollTo(0,0);
	    	}else{
	    		ajax.get(routes[routeName].templateUri)
		    	.done(function(data){
		    		partials.innerHTML = data;
		    		cache[cacheName] = data;
		    		var strUrl ='/';
		    		if(section){
		    			strUrl += section+'/';
		    			if(page){
							strUrl += page+'/';
		    			}
		    		}
		    			IndexController.start();
			    		domReady(function () {
				    		window.history.pushState("", "", strUrl);
				    		window.scrollTo(0,0);
			    		});
		    	})
		    	.error(function(data){
		    		console.log('error - 404')
		    	});
	    	}
	    }
	});

});
