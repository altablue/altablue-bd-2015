requirejs.config({
	baseUrl: '/_assets/js/modules',
	paths : {
		crossroads : '/_assets/js/libs/crossroads/dist/crossroads.min',
		signals : '/_assets/js/libs/crossroads/dev/lib/signals',
		Ajax : '/_assets/js/libs/ajax/dist/ajax.min',
		jquery : '/_assets/js/libs/jquery/dist/jquery.min',
		anystretch : '/_assets/js/libs/anystretch/jquery.anystretch.min',
		gmaps: '/_assets/js/libs/gmaps',
		async: '/_assets/js/libs/async/dist/async.min',
		wow: '/_assets/js/libs/wow/dist/wow',
		menuAim: '/_assets/js/libs/jquery.menu-aim',
		classie: '/_assets/js/libs/classie',
		mlpushmenu: '/_assets/js/libs/mlpushmenu',
		domReady: '/_assets/js/libs/domReady',
		modernizr: '/_assets/js/libs/modernizr.custom',
		Pikaday: '/_assets/js/libs/pikaday/pikaday',
		timepicker: '/_assets/js/libs/jt.timepicker/jquery.timepicker.min',
		underscore : '/_assets/js/libs/underscore/underscore-min'
	},
	shim: {
		anystretch: {
			deps: ["jquery"]
		},
		menuAim: {
			deps: ["jquery"]
		}
	}
});

var debug = true;

require(['homepage/Controllers/IndexController', 'routes', 'router'], function(IndexController, routes, router){
    IndexController.start();
});