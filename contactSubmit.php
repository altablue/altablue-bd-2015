<?php
error_reporting(1);
ini_set('display_errors', 'E_ALL');
require_once("class.simple_mail.php");
$data = array();
parse_str($_POST['formOptions'], $data);

$msg = 'Contact Enquiery from '.ucfirst($data["fullName"]).'<br/>
<br/>
------<br/>
<br/>
'.$data["message"].'<br/>
<br/>
------<br/>
<br/>
Contact deatails:<br/>
<br/>
company name: '.$data["comapnyName"].'<br/>
email: '.$data["emailAddress"].'<br/>
phone: '.$data["phone"].'<br/>
';

$mail = new SimpleMail();
$mail->setTo('callum.hopkins@alta-blue.com','')
     ->setSubject('Contact Enquiry from Alta-blue.com')
     ->setFrom('contactform@alta-blue.com', 'alta-blue.com');

$mail->addMailHeader('Reply-To', $data['emailAddress'], ucfirst($data['fulName']))
     ->addGenericHeader('X-Mailer', 'PHP/' . phpversion())
     ->addGenericHeader('Content-Type', 'text/html; charset="utf-8"')
     ->setMessage($msg)
     ->setWrap(100);

$send = $mail->send();
echo ($send) ? true : 'false';